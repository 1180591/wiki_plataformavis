# UC208 - Submeter nova proposta de voluntariado

## Formato Breve

O responsável da organização inicia o processo de submissão de uma nova proposta. O sistema solicita os dados necessários (i.e. título, descrição, data de início, data de fim, total de horas de voluntariado, local, área de atuação, funções a desempenhar, outras observações). O responsável da organização introduz os dados solicitados. O sistema valida os dados e solicita confirmação. O responsável da organização confirma. O sistema regista os dados da proposta, informa o utilizador do sucesso da operação e redireciona-o para o feed de propostas.

## SSD
![SSD_UC208.png](SSD_UC208.png)


## Formato Completo

### Ator principal

Organização

### Partes interessadas e seus interesses
* **Organização:** pretende submeter a proposta para atrair candidatos.
* **Voluntário:** pretende que sejam submetidas novas propostas de voluntariado do seu interesse. 


### Pré-condições
A organização deve estar registada no sistema.
As áreas de intervenção devem estar na BD.

### Pós-condições
A informação da proposta é guardada no sistema. O estado da proposta será 0, ou seja, pendente.

## Cenário de sucesso principal (ou fluxo básico)

1. O responsável da organização inicia submissão de proposta.
2. O sistema solicita dados (i.e. título, descrição, data de início, data de fim, total de horas de voluntariado, local, área de atuação, funções a desempenhar, outras observações). 
3. O responsável da organização introduz os dados solicitados. 
4. O sistema valida os dados e solicita confirmação.
5. O responsável da organização confirma. 
6. O sistema regista os dados da proposta, informa o utilizador do sucesso da operação e redireciona-o para o feed de propostas.

### Extensões (ou fluxos alternativos)

*a. O responsável da organização solicita o cancelamento da submissão.

> O caso de uso termina.

3a. As áreas de intervenção não são apresentadas na select box.
>   1. O sistema informa que, de momento, não é possível submeter uma nova proposta.
>   2. O sistema regista o erro no respetivo log.
> O caso de uso termina.

4a. As datas não são válidas.
>	1. O sistema informa o motivo da invalidação dos dados.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O responsável da organização não altera os dados. O caso de uso termina.
	
4b. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3).
>
	>	2a. O responsável da organização não altera os dados. O caso de uso termina.

[Voltar atrás](../Home.md)
