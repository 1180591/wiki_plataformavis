Lista de Funcionalidades
===============================


### Índice das Funcionalidade Desenvolvidas ###


| Funcionalidades relativas a: Voluntário                      |
| ------------------------------------------------------------ |
| [US101](../ProcessoEngenharia/US1008_ConsultarProdutosSemFicha/ConsultarProdutosSemFicha.md) Registo ❌ |
| [US102](../ProcessoEngenharia/US2004_EspecificarFichaDeProducao/EspecificarFichaProducao) Autenticação ✅ |
| [US103](../ProcessoEngenharia/US2005_ImportarProdutosDeCSV/ImportarCatalogoProdutos) Feed propostas ✅ |
| [US104](.) Detalhes de proposta ✅                            |
| [US105](./) Perfil de  entidade  ✅                           |
| [US106](.) Enviar candidatura ✅                              |
| [US107]() Feed candidaturas ✅                                |
| [US108](.) Ver perfil ✅                                      |
| [US109]() Editar perfil ✅                                    |
| [US110]() Notificações (novas propostas ✅ e questionários de satisfação ❌) |
| [US111]() Questionário de satisfação ❌                       |
| [US112]()  Newsletter ❌                                      |

| Funcionalidades relativas a: Entidade       |
| ------------------------------------------- |
| [US201]() Registo ❌                         |
| [US202](.) Autenticação ✅                   |
| [US203]() Feed propostas  ✅                 |
| [US204]() Detalhes de proposta  ✅           |
| [US205]() Gestão de candidatos a proposta ❌ |
| [US206]() Editar proposta ✅                 |
| [US207]() Perfil de candidato ✅             |
| [US208](.) Submeter nova proposta ✅         |
| [US209]() Ver perfil ✅                      |
| [US210]() Editar perfil ✅                   |

| Funcionalidades relativas a: Gestor               |
| ------------------------------------------------- |
| [US301]() Registo ❌                               |
| [US302]() Autenticação ✅                          |
| [US303]() Feed propostas  ✅                       |
| [US304]() Detalhes de proposta  ✅                 |
| [US305]() Validação de proposta de voluntariado ❌ |
| [US306]() Feed de organizações ✅                  |
| [US307]() Detalhes de organização  ✅              |
| [US308]() Feed de voluntários ✅                   |
| [US309]() Perfil de voluntário  ✅                 |
| [US310]() Descarregar métricas ❌                  |

