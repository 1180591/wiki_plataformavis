-- Escolas e cursos
INSERT INTO "Schools" ("Name") VALUES ('ISEP');

INSERT INTO "Degrees" ("SchoolId","Name") VALUES ('1','Engenharia Informatica');
INSERT INTO "Degrees" ("SchoolId","Name") VALUES ('1','Engenharia Mecanica');
INSERT INTO "Degrees" ("SchoolId","Name") VALUES ('1','Engenharia Eletrotecnica');

-- AspNetRoles
INSERT INTO "AspNetRoles" ("Id","Name", "NormalizedName", "ConcurrencyStamp") VALUES ('1','Volunteer','Volunteer','');
INSERT INTO "AspNetRoles" ("Id","Name", "NormalizedName", "ConcurrencyStamp") VALUES ('2','Organization','Organization','');

-- Intervention Areas
INSERT INTO "InterventionAreas" ("TituloVoluntario","DescricaoVoluntario","TituloOrganizacao","DescricaoOrganizacao") VALUES (
'Voluntariado Social',
'ação que visa apoiar indivíduos e grupos excluídos ou em risco de exclusão social (séniores, mulheres, jovens, sem abrigo, migrantes, reclusos, etnias e outros); atenção a grupos com necessidades especiais ou deficiências específicas.',
'Social / Social',
'apoio a indivíduos e grupos excluídos ou em risco de exclusão social (séniores, mulheres, jovens, sem abrigo, migrantes, reclusos, etnias e outros); atenção a grupos com necessidades especiais ou deficiências específicas / support to individuals and groups excluded or at risk of social exclusion (seniors, women, young people, homeless, migrants, prisoners, ethnicities and others); attention to groups with special needs or specific disabilities.');

INSERT INTO "InterventionAreas" ("TituloVoluntario","DescricaoVoluntario","TituloOrganizacao","DescricaoOrganizacao") VALUES (
'Voluntariado Cultural',
'ações para a recuperação ou conservação da identidade cultural, a promoção da criatividade, a disseminação de bens culturais.',
'Cultural / Cultural',
'recuperação ou conservação da identidade cultural; promoção da criatividade; disseminação de bens culturais / recovery or conservation of cultural identity; promotion of creativity; dissemination of cultural goods.');

INSERT INTO "InterventionAreas" ("TituloVoluntario","DescricaoVoluntario","TituloOrganizacao","DescricaoOrganizacao") VALUES (
'Voluntariado Educacional',
'são atividades de alfabetização e educação, de defesa da educação de todas as pessoas, de participação nas comunidades escolares.',
'Educacional / Educational',
'alfabetização e educação; defesa da educação de todas as pessoas; participação nas comunidades escolares / literacy and education; defending the education of all people; participation in school communities.');

INSERT INTO "InterventionAreas" ("TituloVoluntario","DescricaoVoluntario","TituloOrganizacao","DescricaoOrganizacao") VALUES (
'Voluntariado em Ambiente',
'ações para a proteção ou recuperação de espécies ou espaços naturais, a localização de situações de degradação ambiental, o cuidado e a proteção de animais, a conscientização e sensibilização da população em geral para a proteção do meio ambiente e a promoção de uma vida sustentável e ambientalmente amigável.',
'Ambiental / Environmental',
'proteção ou recuperação de espécies ou espaços naturais; localização de situações de degradação ambiental; cuidado e proteção de animais; conscientização e sensibilização da população em geral para a proteção do meio ambiente e a promoção de uma vida sustentável e ambientalmente amigável / protection or recovery of species or natural spaces; location of environmental degradation situations; care and protection of animals; awareness and sensitization of the general population for the protection of the environment and the promotion of a sustainable and environmentally friendly life.');

INSERT INTO "InterventionAreas" ("TituloVoluntario","DescricaoVoluntario","TituloOrganizacao","DescricaoOrganizacao") VALUES (
'Voluntariado com Animais',
'apadrinhar animais; passeá-los; proceder a cuidados de higiéne; promover a sua adoção; ajudar nos cuidados veterinários (tratamentos, medicação, transporte ao hospital etc.); participar em campanhas de recolha de alimentos; angariação de fundos e associados; família de acolhimento temporário.',
'Animais / Animals',
'apadrinhamento e adoção de animais; abrigo e tratamento; cuidados veterinários; campanhas de recolha de alimentos e outras necessidades; promoção dos direitos dos animais / sponsorship and adoption of animals; shelter and treatment; veterinary care; campaigns to collect food and other necessities; promotion of animal rights.');

INSERT INTO "InterventionAreas" ("TituloVoluntario","DescricaoVoluntario","TituloOrganizacao","DescricaoOrganizacao") VALUES (
'Voluntariado em Saúde',
'atividades de promoção de hábitos de vida saudáveis, na defesa de saúde de qualidade para todas as pessoas.',
'Saúde / Health',
'atividades de promoção de hábitos de vida saudáveis; defesa da saúde de qualidade para todas as pessoas / activities to promote healthy lifestyle habits; advocating quality health for all people.');

INSERT INTO "InterventionAreas" ("TituloVoluntario","DescricaoVoluntario","TituloOrganizacao","DescricaoOrganizacao") VALUES (
'Voluntariado em Cidadania',
'ações de promoção da participação cívica, fortalecimento de organizações e movimentos sociais, e da promoção de iniciativa social.',
'Cidadania / Citizenship',
'promoção da participação cívica; fortalecimento de organizações e movimentos sociais; promoção de iniciativa social / promoting civic participation; strengthening social organizations and movements; promotion of social initiative.');

INSERT INTO "InterventionAreas" ("TituloVoluntario","DescricaoVoluntario","TituloOrganizacao","DescricaoOrganizacao") VALUES (
'Voluntariado em Direitos Humanos',
'ações destinadas a defender e denunciar violações de direitos humanos, conscientização social sobre a importância dos direitos humanos.',
'Direitos Humanos / Human Rights',
'defensa dos Direitos Humanos; denuncia a violações dos Direitos Humanos; conscientização social sobre a importância dos Direitos Humanos / defense of Human Rights; denounces human rights violations; social awareness of the importance of human rights.');

INSERT INTO "InterventionAreas" ("TituloVoluntario","DescricaoVoluntario","TituloOrganizacao","DescricaoOrganizacao") VALUES (
'Voluntariado em Desenvolvimento Social e Económico',
'ações de promoção social e de desenvolvimento económico ao nível local e comunitário, destinadas a melhorar as condições de vida de uma comunidade ou setor social específico.',
'Desenvolvimento Social e Económico / Social and Economical Development',
'promoção social e de desenvolvimento económico ao nível local e comunitário, destinadas a melhorar as condições de vida de uma comunidade ou setor social específico / social promotion and economic development at local and community level, aimed at improving the living conditions of a specific community or social sector.');

INSERT INTO "InterventionAreas" ("TituloVoluntario","DescricaoVoluntario","TituloOrganizacao","DescricaoOrganizacao") VALUES (
'Voluntariado em Lazer e Tempo Livre',
'ações com crianças e adolescentes para a educação do tempo livre, a promoção de valores, conhecimentos e atitudes que são desenvolvidos nas atividades de lazer.',
'Lazer e Tempo Livre / Leisure and Free Time',
'educação para o tempo livre; promoção de valores, conhecimentos e atitudes que são desenvolvidos nas atividades de lazer / education for free time; promotion of values, knowledge and attitudes that are developed in leisure activities.');

INSERT INTO "InterventionAreas" ("TituloVoluntario","DescricaoVoluntario","TituloOrganizacao","DescricaoOrganizacao") VALUES (
'Voluntariado em Apoio à Gestão das Organizações',
'ações voltadas para as entidades sociais e empresariais que possam necessitar de orientação e apoio à sua gestão nos âmbitos jurídico, técnico, financeiro e organizacional.',
'Apoio à Gestão das Organizações / Support to the Management of Organizations',
'desenvolvimento de ações voltadas para as entidades sociais e empresariais que possam necessitar de orientação e apoio à sua gestão nos âmbitos jurídico, técnico, financeiro e organizacional / development of actions aimed at social and business entities that may need guidance and support to their management in the legal, technical, financial and organizational spheres.');

INSERT INTO "InterventionAreas" ("TituloVoluntario","DescricaoVoluntario","TituloOrganizacao","DescricaoOrganizacao") VALUES (
'Voluntariado em Alívio e Emergência',
'ações destinadas a responder a situações de emergência causadas por desastres naturais, guerras ou outras calamidades.',
'Alívio e Emergência / Relief and Emergency',
'resposta a situações de emergência causadas por desastres naturais, guerras ou outras calamidades / responding to emergency situations caused by natural disasters, wars or other calamities.');

INSERT INTO "InterventionAreas" ("TituloVoluntario","DescricaoVoluntario","TituloOrganizacao","DescricaoOrganizacao") VALUES (
'Voluntariado em Promoção do Voluntariado',
'ações de apoio, divulgação e reconhecimento de voluntários, bem como de promoção de maior participação social nesse tipo de atividades',
'Promoção do Voluntariado / Promotion of Volunteering',
'apoio, divulgação e capacitação de voluntários; promoção de maior participação social neste tipo de atividades / support, dissemination and training of volunteers; promoting greater social participation in these types of activities.');

commit;