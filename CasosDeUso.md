# Diagrama de Casos de Uso

![DiagramaUCs](./Multimedia/DiagramaUCs.png)

# Casos de Uso

| UC     | Descrição                          |
| :----- | :--------------------------------- |
| UC1001 | Registo voluntário                 |
| UC1002 | Listar propostas de voluntariado   |
| UC1003 | Consultar proposta                 |
| UC1004 | Efetuar candidatura                |
| UC1005 | Gerir perfil                       |
| UC2001 | Abertura de Conta                  |
| UC2002 | Submeter proposta de voluntariado  |
| UC2003 | Gerir candidatos a uma proposta    |
| UC2004 | Gerir perfil                       |
| UC3001 | Visualizar pedidos de abertura     |
| UC3002 | Ativar proposta de voluntariado    |
| UC3003 | Desativar proposta de voluntariado |

