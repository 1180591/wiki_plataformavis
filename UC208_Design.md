# UC208 - Submeter nova proposta

DESCRIÇÃO

## 3.1. Realização da Funcionalidade

![UC208_SD](UC208_SD.png)

## 3.2. Diagrama de Classes

![UC208_CD](UC208_CD.png)

## 3.3. Padrões Aplicados

Tal como foi referido no início desta secção, a análise dos produtos foi feita diretamente na base de dados através de uma query SQL ao invés da reconstrução do objeto e análise em memória.

## 3.4. Testes 
Nesta User Story em particular não são necessários testes unitários. Apenas reconstruímos objetos já persistidos na base de dados de modo a poder imprimi-los.

No entanto, foram levados a cabo vários testes funcionais. Passaremos a demonstrar a execução de quatro cenários:

1. **Todos os produtos persistidos na base de dados têm já uma ficha associada**

![US2003_cenario1](US2003_cenario1.png)

2. **Não existem produtos na base de dados**

![US2003_cenario2](US2003_cenario2.png)



3. **Nenhum produto persistido na base de dados tem uma ficha associada**

![US2003_cenario3](US2003_cenario3.png)

4. **Alguns produtos persistidos na base de dados têm ficha, outros não.**

![US2003_cenario4](US2003_cenario4.png)

