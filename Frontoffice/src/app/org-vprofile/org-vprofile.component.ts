import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Application from '../model/Application';
import { ApplicationService } from '../services/application/application.service';
import { LoginService } from '../services/login/login.service';
import { VolunteerService } from '../services/volunteer/volunteer.service';

@Component({
  selector: 'app-org-vprofile',
  templateUrl: './org-vprofile.component.html',
  styleUrls: ['./org-vprofile.component.css']
})
export class OrgVProfileComponent implements OnInit {
  volunteer : any;
  gender : any;
  internationalInterests : any;
  currentApplications: Array<Application> = new Array<Application>();

  constructor(private route: ActivatedRoute,
    private router: Router,
    private applicationsService: ApplicationService,
    private logoutService: LoginService,
    private volunteerService: VolunteerService) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const offerIdFromRoute = Number(routeParams.get('vId'));

    this.getVolunteerById(offerIdFromRoute);
    this.getApplications(offerIdFromRoute);
  }

  getApplications(vId:number) {
    this.applicationsService.getByVolunteerId(vId).subscribe(
      (res:any) => {
        this.currentApplications = res;
      },
      err => { console.log(err) }
    );
  }

  getVolunteerById(vId:number) {
    this.volunteerService.getVolunteerById(vId).subscribe(
      res => {
        this.volunteer = res;
        
        if (this.volunteer.gender == "M") { this.gender = "Masculino" } else { this.gender = "Feminino" }
        if (this.volunteer.internationalInterests) { this.internationalInterests = "Sim" } else { this.internationalInterests = "Não"  } 
      },
      err => {
        console.log(err)
      }
    );
  }

  onLogout() {
    this.logoutService.logout();
  }

  redirectToFeed() {
    this.router.navigateByUrl("/organizationFeed");
  }
  
  redirectToProfile() {
    this.router.navigateByUrl("/oProfile");
  }
}
