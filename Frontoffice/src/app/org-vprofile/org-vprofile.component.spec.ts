import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrgVProfileComponent } from './org-vprofile.component';

describe('OrgVProfileComponent', () => {
  let component: OrgVProfileComponent;
  let fixture: ComponentFixture<OrgVProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrgVProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrgVProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
