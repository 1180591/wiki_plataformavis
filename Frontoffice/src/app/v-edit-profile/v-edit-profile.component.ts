import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Notification from '../model/Notification';
import { GenderService } from '../services/gender/gender.service';
import { IdDocumentTypeService } from '../services/idDocumentType/id-document-type.service';
import { InternationalInterestService } from '../services/internationalInterest/international-interest.service';
import { LoginService } from '../services/login/login.service';
import { NotificationService } from '../services/notification/notification.service';
import { OfferService } from '../services/offer/offer.service';
import { SchoolService } from '../services/school/school.service';
import { VolunteerService } from '../services/volunteer/volunteer.service';

@Component({
  selector: 'app-v-edit-profile',
  templateUrl: './v-edit-profile.component.html',
  styleUrls: ['./v-edit-profile.component.css']
})
export class VEditProfileComponent implements OnInit {
  currentVolunteer : any;
  notifications : Array<Notification> =  new Array<Notification>();
  notificationSentences : Array<string> = new Array<string>();

  schoolsList : any;
  degreesList : any;
  gendersList : any;
  internationalInterestsAnswersList : any;
  idDocumentTypesList : any;

  currentName : any;
  currentGender : any;
  currentBirthday : any;
  currentNationality : any;
  currentIdDocumentType : any;
  currentIdDocumentNumber : any;
  currentTaxpayerNumber : any;
  currentEmailAddress : any;
  currentPhoneNumber : any;
  currentEmergencyPhoneNumber : any;
  currentAddress : any;
  currentZipCode : any;
  currentLocality : any;
  currentMechanographicNumber : any;
  currentSchool : any;
  currentDegree : any;
  currentExperience : any;
  currentReasons : any;
  currentInternationalInterests : any;
  currentAdditionalInfo : any;

  constructor(
    private router : Router,
    private volunteerService : VolunteerService,
    private logoutService : LoginService,
    private schoolService : SchoolService,
    private genderService : GenderService,
    private internationalInterestService : InternationalInterestService,
    private idDocumentTypeService : IdDocumentTypeService,
    private notificationService : NotificationService,
    private offerService : OfferService
  ) { }

  ngOnInit(): void {
    this.getVolunteerData();
    this.getSchools();
    this.getDegrees();
    this.getGenders();
    this.getInternationalInterestAnswers();
    this.getIdDocumentTypes();
  }

  getGenders() {
    this.genderService.getGenders().subscribe(
      res => { this.gendersList = res; },
      err => { console.log(err) }
    );
  }

  getInternationalInterestAnswers() {
    this.internationalInterestService.getAnswers().subscribe(
      res => { this.internationalInterestsAnswersList = res; },
      err => { console.log(err) }
    );
  }

  getIdDocumentTypes() {
    this.idDocumentTypeService.getDocumentTypes().subscribe(
      res => { this.idDocumentTypesList = res; },
      err => { console.log(err) }
    );
  }

  getVolunteerData() {
    this.volunteerService.getVolunteerProfile().subscribe(
      res => {
        this.currentVolunteer = res;

        this.notificationService.getNotificationsByVolunteerId(this.currentVolunteer.id).subscribe(
          (res:any) => {
            this.notifications = res;
            var nNotifications = this.notifications.length;
            for (var i = 0; i < nNotifications; i++) {
              var not = this.notifications[i];
              var idOffer = not.idOffer;
              var isNewOffer = not.isNewOffer;

              if (isNewOffer) { // nova proposta
                this.offerService.getById(idOffer).subscribe(
                  (res:any) => { this.notificationSentences.push(res.title) },
                  err => { console.log(err) }
                );
              } else { // questionario
                this.offerService.getById(idOffer).subscribe(
                  (res:any) => { this.notificationSentences.push(res.organizationName) },
                  err => { console.log(err) }
                );
              }


            }
          },
          err => {console.log(err)}
        );

        this.currentName = this.currentVolunteer.fullName;
        this.currentGender = this.currentVolunteer.genderId;
        this.currentBirthday = this.currentVolunteer.birthday;
        this.currentNationality = this.currentVolunteer.nationality;
        this.currentIdDocumentType = this.currentVolunteer.identificationDocumentTypeId;
        this.currentIdDocumentNumber = this.currentVolunteer.identificationNumber;
        this.currentTaxpayerNumber = this.currentVolunteer.taxpayerNumber;
        this.currentEmailAddress = this.currentVolunteer.emailAddress;
        this.currentPhoneNumber = this.currentVolunteer.phoneNumber;
        this.currentEmergencyPhoneNumber = this.currentVolunteer.emergencyPhoneNumber;
        this.currentAddress = this.currentVolunteer.address;
        this.currentZipCode = this.currentVolunteer.postalCode;
        this.currentLocality = this.currentVolunteer.location;
        this.currentMechanographicNumber = this.currentVolunteer.mechanographicNumber;
        this.currentSchool = this.currentVolunteer.schoolId;
        this.currentDegree = this.currentVolunteer.degreeId;
        this.currentExperience = this.currentVolunteer.volunteeringExperience;
        this.currentReasons = this.currentVolunteer.reasonsToVolunteer;
        this.currentInternationalInterests = this.currentVolunteer.internationalInterestId;
        this.currentAdditionalInfo = this.currentVolunteer.additionalInformation;
      },
      err => { console.log(err) }
    );
  }

  getSchools() {
    this.schoolService.getAllSchools().subscribe(
      res => {
        this.schoolsList = res;
      },
      err => {console.log(err)}
    );
  }

  getDegrees() {
    this.schoolService.getAllDegrees().subscribe(
      res => {
        this.degreesList = res;
        console.log(this.degreesList.length);
      },
      err => {console.log(err)}
    );
  }

  logout() {
    this.logoutService.logout();
  }

  redirectToProfile() {
    this.router.navigate(['/vProfile',this.currentVolunteer.id]);
  }

  redirectToApplicationList() {
    this.router.navigate(['/vApplications',this.currentVolunteer.id]);
  }

  editProfile() {
    var dto = {
      Id : this.currentVolunteer.id,
      FullName : this.currentName,
      GenderId : this.currentGender,
      Birthday : this.currentBirthday,
      Nationality : this.currentNationality,
      IdentificationDocumentTypeId : this.currentIdDocumentType,
      IdentificationNumber : this.currentIdDocumentNumber,
      TaxpayerNumber : this.currentTaxpayerNumber,
      EmailAddress : this.currentEmailAddress,
      PhoneNumber : this.currentPhoneNumber,
      EmergencyPhoneNumber : this.currentEmergencyPhoneNumber,
      Address : this.currentAddress,
      PostalCode : this.currentZipCode,
      Location : this.currentLocality,
      MechanographicNumber : this.currentMechanographicNumber,
      SchoolId : this.currentSchool,
      DegreeId : this.currentDegree,
      VolunteeringExperience : this.currentExperience,
      ReasonsToVolunteer : this.currentReasons,
      InternationalInterestId : this.currentInternationalInterests,
      AdditionalInformation : this.currentAdditionalInfo
    }

    console.log(dto);

    this.volunteerService.editVolunteerProfile(dto).subscribe(
      res => {
        this.router.navigateByUrl('/volunteeringFeed');
      },
      err => {}
    );

  }

  redirectToFeed() {
    this.router.navigateByUrl("/volunteeringFeed");
  }

  changeName() { this.currentName = (document.getElementById("nome") as HTMLInputElement).value; }
  changeSchool() { this.currentSchool = (document.getElementById("escola") as HTMLInputElement).value; }
  changeDegree() { this.currentDegree = (document.getElementById("curso") as HTMLInputElement).value; }
  changeMechanographicNumber() { this.currentMechanographicNumber = (document.getElementById("n_mecanografico") as HTMLInputElement).value; }
  changeNationality() { this.currentNationality = (document.getElementById("nacionalidade") as HTMLInputElement).value; }
  changeGender() { this.currentGender = (document.getElementById("sexo") as HTMLInputElement).value; }
  changeVolunteeringExperience() { this.currentExperience = (document.getElementById("experiencia") as HTMLInputElement).value; }
  changeReasonsToVolunteer() { this.currentReasons = (document.getElementById("razoes") as HTMLInputElement).value; }
  changeInternationalInterests() { this.currentInternationalInterests = (document.getElementById("internacional") as HTMLInputElement).value; }
  changeAdditionalInfo() { this.currentAdditionalInfo = (document.getElementById("info_adicionais") as HTMLInputElement).value; }
  changeEmailAddress() { this.currentEmailAddress = (document.getElementById("email") as HTMLInputElement).value; }
  changePhoneNumber() { this.currentPhoneNumber = (document.getElementById("telemovel") as HTMLInputElement).value; }
  changeEmergencyPhoneNumber() { this.currentEmergencyPhoneNumber = (document.getElementById("contacto_emergencia") as HTMLInputElement).value; }
  changeAddress() { this.currentAddress = (document.getElementById("morada") as HTMLInputElement).value; }
  changeZipCode() { this.currentZipCode = (document.getElementById("codigo_postal") as HTMLInputElement).value; }
  changeLocation() { this.currentLocality = (document.getElementById("localidade") as HTMLInputElement).value; }
  changeIdDocumentType() { this.currentIdDocumentType = (document.getElementById("documento") as HTMLInputElement).value; }
  changeIdDocumentNumber() { this.currentIdDocumentNumber = (document.getElementById("n_documento") as HTMLInputElement).value; }
  changeTaxpayerNumber() { this.currentTaxpayerNumber = (document.getElementById("nif") as HTMLInputElement).value; }
  changeBirthday() { this.currentBirthday = (document.getElementById("data_nascimento") as HTMLInputElement).value; }

  offerDetails(offerId : number) {
    this.router.navigate(['/vOfferDetails',offerId]);
  }

  readNewOfferNotification(notification : any) {
    this.offerDetails(notification.idOffer);
    this.notificationService.read(notification.id).subscribe(
      res => {console.log("Notificacao lida!")},
      err => {console.log(err)}
    );
  }
}
