import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VEditProfileComponent } from './v-edit-profile.component';

describe('VEditProfileComponent', () => {
  let component: VEditProfileComponent;
  let fixture: ComponentFixture<VEditProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VEditProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VEditProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
