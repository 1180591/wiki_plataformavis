import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AMetricsComponent } from './a-metrics/a-metrics.component';
import { AOfferDetailsComponent } from './a-offer-details/a-offer-details.component';
import { AOffersComponent } from './a-offers/a-offers.component';
import { AOProfileComponent } from './a-oprofile/a-oprofile.component';
import { AOrganizationsComponent } from './a-organizations/a-organizations.component';
import { AVolunteersComponent } from './a-volunteers/a-volunteers.component';
import { AVProfileComponent } from './a-vprofile/a-vprofile.component';
import { AuthGuard } from './auth/auth.guard';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { NewOfferComponent } from './new-offer/new-offer.component';
import { OEditOfferComponent } from './o-edit-offer/o-edit-offer.component';
import { OEditProfileComponent } from './o-edit-profile/o-edit-profile.component';
import { OOfferDetailsComponent } from './o-offer-details/o-offer-details.component';
import { OProfileComponent } from './o-profile/o-profile.component';
import { OrgVProfileComponent } from './org-vprofile/org-vprofile.component';
import { OrganizationFeedComponent } from './organization-feed/organization-feed.component';
import { RegisterO1Component } from './register-o1/register-o1.component';
import { RegisterO2Component } from './register-o2/register-o2.component';
import { RegisterV1Component } from './register-v1/register-v1.component';
import { RegisterV2Component } from './register-v2/register-v2.component';
import { RegisterV3Component } from './register-v3/register-v3.component';
import { RegisterV4Component } from './register-v4/register-v4.component';
import { RegisterComponent } from './register/register.component';
import { VApplicationsComponent } from './v-applications/v-applications.component';
import { VEditProfileComponent } from './v-edit-profile/v-edit-profile.component';
import { VOfferDetailsComponent } from './v-offer-details/v-offer-details.component';
import { VProfileComponent } from './v-profile/v-profile.component';
import { VolunteeringFeedComponent } from './volunteering-feed/volunteering-feed.component';
import { VProfileOrganizationComponent } from './vprofile-organization/vprofile-organization.component';

const routes: Routes = [
  {path:'', redirectTo:'/home',pathMatch:'full'},
  {path:'home', component:HomeComponent}, 
  {path:'register', component:RegisterComponent},
  {path:'registerVolunteer1', component:RegisterV1Component},
  {path:'registerVolunteer2', component:RegisterV2Component},
  {path:'registerVolunteer3', component:RegisterV3Component},
  {path:'registerVolunteer4', component:RegisterV4Component},
  {path:'login', component:LoginComponent},
  {path:'volunteeringFeed', component:VolunteeringFeedComponent, canActivate:[AuthGuard]},
  {path:'organizationFeed', component:OrganizationFeedComponent, canActivate:[AuthGuard]},
  {path:'registerOrganization1', component:RegisterO1Component},
  {path:'registerOrganization2', component:RegisterO2Component},
  {path:'newOffer', component:NewOfferComponent},
  {path:'vOfferDetails/:offerId', component:VOfferDetailsComponent},
  {path:'oOfferDetails/:offerId', component:OOfferDetailsComponent},
  {path:'oProfile/:oId', component:OProfileComponent},
  {path:'vProfileOrg/:oId', component:VProfileOrganizationComponent},
  {path:'vProfile/:vId', component:VProfileComponent},
  {path:'oProfileVol/:vId', component:OrgVProfileComponent},
  {path:'vApplications/:vId', component:VApplicationsComponent},
  {path:'vEditProfile', component:VEditProfileComponent},
  {path:'oEditProfile', component:OEditProfileComponent},
  {path:'editOffer/:offerId', component:OEditOfferComponent},
  {path:'aOffers', component:AOffersComponent},
  {path:'aOfferDetails/:offerId', component:AOfferDetailsComponent},
  {path:'aMetrics', component:AMetricsComponent},
  {path:'aOrganizations', component:AOrganizationsComponent},
  {path:'aOrgProfile/:oId', component:AOProfileComponent},
  {path:'aVolunteers', component:AVolunteersComponent},
  {path:'aVProfile', component:AVProfileComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
