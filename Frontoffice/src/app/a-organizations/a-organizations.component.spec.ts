import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AOrganizationsComponent } from './a-organizations.component';

describe('AOrganizationsComponent', () => {
  let component: AOrganizationsComponent;
  let fixture: ComponentFixture<AOrganizationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AOrganizationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AOrganizationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
