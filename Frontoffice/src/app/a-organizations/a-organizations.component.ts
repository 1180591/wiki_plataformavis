import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Organization from '../model/Organization';
import { LoginService } from '../services/login/login.service';
import { OrganizationService } from '../services/organization/organization.service';

@Component({
  selector: 'app-a-organizations',
  templateUrl: './a-organizations.component.html',
  styleUrls: ['./a-organizations.component.css']
})
export class AOrganizationsComponent implements OnInit {

  organizations : Array<Organization> = new Array<Organization>();

  constructor(
    private router : Router,
    private organizationService : OrganizationService,
    private loginService : LoginService
  ) { }

  ngOnInit(): void {
    this.organizationService.getAll().subscribe(
      (res:any) => {
        this.organizations = res;
      },
      err => { console.log(err) }
    );
  }

  redirectToOrganizations() {
    this.router.navigateByUrl("/aOrganizations");
  }

  redirectToOffers() {
    this.router.navigateByUrl("/aOffers");
  }
  
  redirectToVolunteers() {
    this.router.navigateByUrl("/aVolunteers");
  }

  redirectToMetrics() {
    this.router.navigateByUrl('/aMetrics');
  }

  redirectToOrganizationProfile(oId : number) {
    this.router.navigate(['/aOrgProfile',oId]);
  }

  onLogout() {
    this.loginService.logout();
  }
}
