import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Offer from '../model/Offer';
import Organization from '../model/Organization';
import { InterventionAreasService } from '../services/interventionAreas/intervention-areas.service';
import { LoginService } from '../services/login/login.service';
import { OfferService } from '../services/offer/offer.service';
import { OrganizationService } from '../services/organization/organization.service';

@Component({
  selector: 'app-o-profile',
  templateUrl: './o-profile.component.html',
  styleUrls: ['./o-profile.component.css']
})
export class OProfileComponent implements OnInit {
  organization : any;
  interventionAreas : Array<string> = new Array<string>();
  offerHistory : Array<Offer> = new Array<Offer>();
  offersStatus : Array<number> = new Array<number>();

  constructor(private router: Router, 
    private logoutService: LoginService, 
    private organizationService: OrganizationService, 
    private interventionAreasService: InterventionAreasService,
    private offerService: OfferService) { }

  ngOnInit(): void {
    this.organizationService.getOrganizationProfile().subscribe(
      (res:any) => {
        this.organization = res;
        var interventionAreasIds : number[] = this.organization.interventionAreasIds;
        var nAreas = interventionAreasIds.length;

        for (var i = 0; i < nAreas; i++) {
          var areaId = interventionAreasIds[i];
          console.log("Area com ID "+areaId+"...");
          this.interventionAreasService.getInterventionAreaById(areaId).subscribe(
            (res:any) => { 
              this.interventionAreas.push(res.tituloVoluntario); 
            },
            err => {
              console.log(err)
            }
          );
        };

        this.offerService.getOffersByOrganizationId(this.organization.id).subscribe(
          (res:any) => {
            this.offerHistory = res;
            var nOffers = this.offerHistory.length;
            
            var today = new Date();
            var dd = today.getDate()
            var mm = today.getMonth(); //January is 0!
            var yyyy = today.getFullYear();            

            for (var i = 0; i < nOffers; i++) {

              var offerStartDate = this.offerHistory[i].startDate.split("-");
                  var offerStartDay = parseInt(offerStartDate[2],10);
                  var offerStartMonth = parseInt(offerStartDate[1],10);
                  var offerStartYear = parseInt(offerStartDate[0],10);

              var offerEndDate = this.offerHistory[i].startDate.split("-");
                  var offerEndDay = parseInt(offerStartDate[2],10);
                  var offerEndMonth = parseInt(offerStartDate[1],10);
                  var offerEndYear = parseInt(offerStartDate[0],10);

              if (this.offerService.offerAlreadyEnded(yyyy,mm,dd,offerEndYear,offerEndMonth,offerEndDay)) {
                this.offersStatus[i] = 0; break;
              } else if (this.offerService.offerAlreadyStarted(yyyy,mm,dd,offerStartYear,offerStartMonth,offerStartDay)) {
                this.offersStatus[i] = 1;
              }
            }
          },
          err => { console.log(err); }
        );
      },
      err => {
        console.log(err)
      }
    );
  }

  editProfile() {
    this.router.navigateByUrl('/oEditProfile');
  }

  onLogout() {
    this.logoutService.logout();
  }

  redirectToFeed() {
    this.router.navigate(['/organizationFeed']);
  }
}
