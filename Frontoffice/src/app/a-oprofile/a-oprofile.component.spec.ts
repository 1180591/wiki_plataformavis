import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AOProfileComponent } from './a-oprofile.component';

describe('AOProfileComponent', () => {
  let component: AOProfileComponent;
  let fixture: ComponentFixture<AOProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AOProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AOProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
