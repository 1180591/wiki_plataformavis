import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Offer from '../model/Offer';
import { InterventionAreasService } from '../services/interventionAreas/intervention-areas.service';
import { LoginService } from '../services/login/login.service';
import { OfferService } from '../services/offer/offer.service';
import { OrganizationService } from '../services/organization/organization.service';
import { VolunteerService } from '../services/volunteer/volunteer.service';

@Component({
  selector: 'app-a-oprofile',
  templateUrl: './a-oprofile.component.html',
  styleUrls: ['./a-oprofile.component.css']
})
export class AOProfileComponent implements OnInit {

  organization : any;
  interventionAreas : Array<string> = new Array<string>();
  offerHistory : Array<Offer> = new Array<Offer>();
  offersStatus : Array<number> = new Array<number>();
  volunteerId : any;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private logoutService: LoginService,
    private organizationService: OrganizationService,
    private interventionAreasService: InterventionAreasService,
    private offerService : OfferService,
    private volunteerService : VolunteerService) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const offerIdFromRoute = Number(routeParams.get('oId'));
    console.log("Id organzation: "+offerIdFromRoute);
    this.getOrganizationById(offerIdFromRoute);
    this.getVolunteerId();
  }

  getOrganizationById(vId:number) {
    this.organizationService.getOrganizationById(vId).subscribe(
      (res:any) => {
        this.organization = res;
        var interventionAreasIds : number[] = this.organization.interventionAreasIds;
        var nAreas = interventionAreasIds.length;

        for (var i = 0; i < nAreas; i++) {
          var areaId = interventionAreasIds[i];
          console.log("Area com ID "+areaId+"...");
          this.interventionAreasService.getInterventionAreaById(areaId).subscribe(
            (res:any) => { 
              this.interventionAreas.push(res.tituloVoluntario); 
            },
            err => {
              console.log(err)
            }
          );
        };

        this.offerService.getOffersByOrganizationId(this.organization.id).subscribe(
          (res:any) => {
            this.offerHistory = res;
            var nOffers = this.offerHistory.length;
            
            var today = new Date();
            var dd = today.getDate()
            var mm = today.getMonth(); //January is 0!
            var yyyy = today.getFullYear();            

            for (var i = 0; i < nOffers; i++) {

              var offerStartDate = this.offerHistory[i].startDate.split("-");
                  var offerDay = parseInt(offerStartDate[2],10);
                  var offerMonth = parseInt(offerStartDate[1],10);
                  var offerYear = parseInt(offerStartDate[0],10);

              if (this.offerService.offerAlreadyEnded(yyyy,mm,dd,offerYear,offerMonth,offerYear)) {
                this.offersStatus[i] = 0; break;
              } else if (this.offerService.offerAlreadyStarted(yyyy,mm,dd,offerYear,offerMonth,offerYear)) {
                this.offersStatus[i] = 1;
              }
            }
          },
          err => { console.log(err); }
        );
      },
      err => {
        console.log(err)
      }
    );
  }

  onLogout() {
    this.logoutService.logout();
  }

  redirectToFeed() {
    this.router.navigate(['/volunteeringFeed']);
  }

  redirectToProfile() {
    this.router.navigate(['/vProfile',this.volunteerId]);
  }

  getVolunteerId() {
    this.volunteerService.getVolunteerId().subscribe(
      res => { this.volunteerId = res; },
      err => { console.log(err) }
    );
  }

}
