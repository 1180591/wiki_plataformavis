import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuard } from './auth/auth.guard';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RegisterV1Component } from './register-v1/register-v1.component';
import { RegisterV2Component } from './register-v2/register-v2.component';
import { RegisterV3Component } from './register-v3/register-v3.component';
import { RegisterV4Component } from './register-v4/register-v4.component';
import { LoginComponent } from './login/login.component';
import { VolunteeringFeedComponent } from './volunteering-feed/volunteering-feed.component';
import { OrganizationFeedComponent } from './organization-feed/organization-feed.component';
import { RegisterO1Component } from './register-o1/register-o1.component';
import { RegisterO2Component } from './register-o2/register-o2.component';
import { NewOfferComponent } from './new-offer/new-offer.component';
import { VOfferDetailsComponent } from './v-offer-details/v-offer-details.component';
import { RouterModule } from '@angular/router';
import { OOfferDetailsComponent } from './o-offer-details/o-offer-details.component';
import { OProfileComponent } from './o-profile/o-profile.component';
import { VProfileComponent } from './v-profile/v-profile.component';
import { OrgVProfileComponent } from './org-vprofile/org-vprofile.component';
import { VApplicationsComponent } from './v-applications/v-applications.component';
import { VProfileOrganizationComponent } from './vprofile-organization/vprofile-organization.component';
import { VEditProfileComponent } from './v-edit-profile/v-edit-profile.component';
import { OEditProfileComponent } from './o-edit-profile/o-edit-profile.component';
import { OEditOfferComponent } from './o-edit-offer/o-edit-offer.component';
import { AOffersComponent } from './a-offers/a-offers.component';
import { AVolunteersComponent } from './a-volunteers/a-volunteers.component';
import { AOrganizationsComponent } from './a-organizations/a-organizations.component';
import { AOProfileComponent } from './a-oprofile/a-oprofile.component';
import { AVProfileComponent } from './a-vprofile/a-vprofile.component';
import { AOfferDetailsComponent } from './a-offer-details/a-offer-details.component';
import { AMetricsComponent } from './a-metrics/a-metrics.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RegisterV1Component,
    RegisterV2Component,
    RegisterV3Component,
    RegisterV4Component,
    LoginComponent,
    VolunteeringFeedComponent,
    OrganizationFeedComponent,
    RegisterO1Component,
    RegisterO2Component,
    NewOfferComponent,
    VOfferDetailsComponent,
    OOfferDetailsComponent,
    OProfileComponent,
    VProfileComponent,
    OrgVProfileComponent,
    VApplicationsComponent,
    VProfileOrganizationComponent,
    VEditProfileComponent,
    OEditProfileComponent,
    OEditOfferComponent,
    AOffersComponent,
    AVolunteersComponent,
    AOrganizationsComponent,
    AOProfileComponent,
    AVProfileComponent,
    AOfferDetailsComponent,
    AMetricsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path:'volunteeringFeed', component:VolunteeringFeedComponent, canActivate:[AuthGuard]},
      {path:'vOfferDetails/:offerId', component:VOfferDetailsComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
