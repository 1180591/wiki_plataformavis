export default interface Volunteer {
    id: number;
    fullName: string;
    gender: string;
    birthday: string;
    nationality: string;
    identificationDocument: string;
    identificationNumber: string;
    taxpayerNumber: string;
    emailAddress: string;
    phoneNumber: string;
    emergencyPhoneNumber: string;
    address: string;
    postalCode: string;
    location: string;
    mechanographicNumber: string;
    schoolId: number;
    degreeId: number;
    volunteeringExperience: string;
    reasonsToVolunteer: string;
    internationInterests: boolean;
    additionalInformation: string;
}