export default interface Offer {
    id: number;
    idOrganization: number;
    organizationName: string;
    organizationLocation: string;
    title: string;
    description: string;
    startDate: string;
    endDate: string;
    expectedVolunteeringHours: number;
    location: string;
    rolesToPlay: string;
    additionalInfo: string;
    interventionAreaId: number;
    status : number;

}