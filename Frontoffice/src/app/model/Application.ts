export default interface Application {
    id: number,
    idOffer: number,
    idVolunteer: number,
    selected: number,
    offerTitle: string,
    offerOrganizationName: string,
    offerDescription: string
}