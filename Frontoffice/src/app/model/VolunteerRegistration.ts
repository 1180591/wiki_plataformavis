export interface VolunteerRegistration {
    name: string,
    gender: string,
    birthday: Date,
    nationality: string,
    idDoc: string,
    idNumber: string,
    taxpayerNumber: string,
    email: string,
    phoneNumber: string,
    postalAddress: string,
    zipCode: string,
    locality: string,
    emergencyPhone: string,
    password: string
}