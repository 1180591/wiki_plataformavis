export default interface Organization {
    id: number;
    emailAddress: string;
    name: string;
    operatingCountries: string;
    websiteUrl: string;
    additionalInfo: string;
    representativeName: string;
    phoneNumber: string;
    address: string;
    postalCode: string;
    location: string;
    interventionAreasIds: number[];
}