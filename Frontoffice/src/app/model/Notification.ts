export default interface Notification {
    id : number;
    idOffer : number;
    idVolunteer : number;
    read : number;
    isSatisfactionForm : number;
    isNewOffer : number;
    organizationName : string;
    offerTitle : string;
}