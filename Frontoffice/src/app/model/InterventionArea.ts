export default interface InterventionArea {
    id: number;
    tituloVoluntario: string;
    descricaoVoluntario: string;
    tituloOrganizacao: string;
    descricaoOrganizacao: string;
}