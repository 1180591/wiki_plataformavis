import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AVolunteersComponent } from './a-volunteers.component';

describe('AVolunteersComponent', () => {
  let component: AVolunteersComponent;
  let fixture: ComponentFixture<AVolunteersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AVolunteersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AVolunteersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
