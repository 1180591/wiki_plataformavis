import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Volunteer from '../model/Volunteer';
import { LoginService } from '../services/login/login.service';
import { VolunteerService } from '../services/volunteer/volunteer.service';

@Component({
  selector: 'app-a-volunteers',
  templateUrl: './a-volunteers.component.html',
  styleUrls: ['./a-volunteers.component.css']
})
export class AVolunteersComponent implements OnInit {

  volunteers : Array<Volunteer> = new Array<Volunteer>();

  constructor(
    private router: Router,
    private volunteerService: VolunteerService,
    private loginService: LoginService
  ) { }

  ngOnInit(): void {
    this.volunteerService.getAll().subscribe(
      (res:any) => { this.volunteers = res; },
      err => { console.log(err) }
    );
  }

  redirectToVolunteerProfile(vId : number) {
    this.router.navigate(['oProfileVol',vId]);
  }

  redirectToOffers() {
    this.router.navigateByUrl('/aOffers');
  }

  redirectToOrganizations() {
    this.router.navigateByUrl('/aOrganizations');
  }

  redirectToVolunteers() {
    this.router.navigateByUrl('/aVolunteers');
  }

  redirectToMetrics() {
    this.router.navigateByUrl('/aMetrics');
  }

  onLogout() {
    this.loginService.logout();
  }

}
