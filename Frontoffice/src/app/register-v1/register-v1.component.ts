import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { VolunteerRegistration } from '../model/VolunteerRegistration';
import { RegistrationService } from '../services/registration/registration.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-register-v1',
  templateUrl: './register-v1.component.html',
  styleUrls: ['./register-v1.component.css']
})
export class RegisterV1Component implements OnInit {

  registerForm;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.registerForm = this.formBuilder.group({
      name: '',
      gender: '',
      birthday: '',
      nationality: '',
      idDoc: '',
      idNumber: '',
      taxpayerNumber: '',
      email: '',
      phoneNumber: '',
      postalAddress: '',
      zipCode: '',
      locality: '',
      emergencyPhone: '',
      password: ''
    });
  }

  ngOnInit(): void {
  }
  
  redirectToNextPage() {
    this.router.navigateByUrl("/registerVolunteer2");
  }

  onSubmit(registration : VolunteerRegistration) {
    /*console.log('Name: '+registration.name);
    console.log('Gender: '+registration.gender);
    console.log('Birthday: '+registration.birthday);
    console.log('Doc: '+registration.idDoc);
    this.registerService.addVolunteer(registration).subscribe(
      response => this.router.navigateByUrl('/volunteerHome'),
      error => console.log('Error on form submit.')
    );*/
  }

}
