import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterV4Component } from './register-v4.component';

describe('RegisterV4Component', () => {
  let component: RegisterV4Component;
  let fixture: ComponentFixture<RegisterV4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterV4Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterV4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
