import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterO1Component } from './register-o1.component';

describe('RegisterO1Component', () => {
  let component: RegisterO1Component;
  let fixture: ComponentFixture<RegisterO1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterO1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterO1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
