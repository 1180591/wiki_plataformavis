import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-o1',
  templateUrl: './register-o1.component.html',
  styleUrls: ['./register-o1.component.css']
})
export class RegisterO1Component implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  redirectToNextPage() {
    this.router.navigateByUrl("/registerOrganization2");
  }

}
