import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../services/login/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  formModel = {
    Email : '',
    Password : ''
  };

  

  constructor(private service : LoginService, private router : Router) { }

  ngOnInit(): void {
    if (localStorage.getItem('token') != null) {
      this.router.navigateByUrl("/volunteeringFeed");
    }
  }

  redirectToRegister() {
    this.router.navigateByUrl("/register");
  }

  onSubmit(form : NgForm) {
    console.log("Login button pressed! ");
    this.service.login(form.value).subscribe(
      (res:any) => {
        console.log("Authentication success! ("+res.role+")");
        localStorage.setItem('token',res.token);
        if (res.role == "Volunteer") {
          this.router.navigateByUrl('/volunteeringFeed');
        } else if (res.role == "Organization") {
          this.router.navigateByUrl('/organizationFeed');
        } else if (res.role == "Admin") {
          this.router.navigateByUrl('/aOffers');
        }
      },
      err => {
        console.log("Authentication error.");
        if (err.status ==  400) { console.log('Authentication error. Wrong email/password.'); }
        else { console.log(err); }
      }
    );
  }
}
