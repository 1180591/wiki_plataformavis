import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AVProfileComponent } from './a-vprofile.component';

describe('AVProfileComponent', () => {
  let component: AVProfileComponent;
  let fixture: ComponentFixture<AVProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AVProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AVProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
