import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationService } from '../services/application/application.service';
import { LoginService } from '../services/login/login.service';
import { VolunteerService } from '../services/volunteer/volunteer.service';
import Application from '../model/Application';
import { SchoolService } from '../services/school/school.service';
import { NotificationService } from '../services/notification/notification.service';
import { OfferService } from '../services/offer/offer.service';
import Notification from '../model/Notification';

@Component({
  selector: 'app-v-profile',
  templateUrl: './v-profile.component.html',
  styleUrls: ['./v-profile.component.css']
})
export class VProfileComponent implements OnInit {
  volunteer : any;
  degreeDto : any;
  gender : any;
  internationalInterests : any;
  vApplications : Array<Application> = new Array<Application>();
  notifications : Array<Notification> =  new Array<Notification>();
  notificationSentences : Array<string> = new Array<string>();

  constructor(private route: ActivatedRoute, 
    private router : Router,
    private volunteerService : VolunteerService,
    private logoutService : LoginService,
    private applicationService : ApplicationService,
    private schoolService : SchoolService,
    private notificationService : NotificationService,
    private offerService : OfferService) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const offerIdFromRoute = Number(routeParams.get('vId'));

    this.getVolunteerById(offerIdFromRoute);
  }

  getVolunteerById(vId:number) {
    this.volunteerService.getVolunteerById(vId).subscribe(
      res => {
        this.volunteer = res;
        
        if (this.volunteer.gender == "M") { this.gender = "Masculino" } else { this.gender = "Feminino" }
        if (this.volunteer.internationalInterests) { this.internationalInterests = "Sim" } else { this.internationalInterests = "Não"  } 

        this.schoolService.getDegreeById(this.volunteer.degreeId).subscribe(
          res => {
            console.log("Vou procurar info sobre o curso com ID "+this.volunteer.degreeId);
            this.degreeDto = res;
            console.log("Nome do curso: "+this.degreeDto.degreeName);
          },
          err => {console.log(err)}
        );

        this.applicationService.getByVolunteerId(vId).subscribe(
          (res:any)=> {
            this.vApplications = res;
          },
          err => { console.log(err); }
        );

        this.notificationService.getNotificationsByVolunteerId(this.volunteer.id).subscribe(
          (res:any) => {
            this.notifications = res;
            var nNotifications = this.notifications.length;
            for (var i = 0; i < nNotifications; i++) {
              var not = this.notifications[i];
              var idOffer = not.idOffer;
              var isNewOffer = not.isNewOffer;

              if (isNewOffer) { // nova proposta
                this.offerService.getById(idOffer).subscribe(
                  (res:any) => { this.notificationSentences.push(res.title) },
                  err => { console.log(err) }
                );
              } else { // questionario
                this.offerService.getById(idOffer).subscribe(
                  (res:any) => { this.notificationSentences.push(res.organizationName) },
                  err => { console.log(err) }
                );
              }


            }
          },
          err => {console.log(err)}
        );
      },
      err => {
        console.log(err)
      }
    );
  }

  onLogout() {
    this.logoutService.logout();
  }

  redirectToFeed() {
    this.router.navigateByUrl("/volunteeringFeed");
  }

  redirectToApplicationList() {
    this.router.navigate(['/vApplications',this.volunteer.id]);
  }

  editProfile() {
    this.router.navigateByUrl("/vEditProfile");
  }

  offerDetails(offerId : number) {
    this.router.navigate(['/vOfferDetails',offerId]);
  }

  readNewOfferNotification(notification : any) {
    this.offerDetails(notification.idOffer);
    this.notificationService.read(notification.id).subscribe(
      res => {console.log("Notificacao lida!")},
      err => {console.log(err)}
    );
  }
}
