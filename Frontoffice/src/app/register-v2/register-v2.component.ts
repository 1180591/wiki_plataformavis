import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-v2',
  templateUrl: './register-v2.component.html',
  styleUrls: ['./register-v2.component.css']
})
export class RegisterV2Component implements OnInit {

  constructor(private router : Router) { }

  ngOnInit(): void {
  }

  redirectToNextPage() {
    this.router.navigateByUrl("/registerVolunteer3");
  }

  
}
