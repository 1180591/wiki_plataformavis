import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import InterventionArea from '../model/InterventionArea';
import Notification from '../model/Notification';
import Offer from '../model/Offer';
import { InterventionAreasService } from '../services/interventionAreas/intervention-areas.service';
import { LoginService } from '../services/login/login.service';
import { NotificationService } from '../services/notification/notification.service';
import { OfferService } from '../services/offer/offer.service';
import { VolunteerService } from '../services/volunteer/volunteer.service';



@Component({
  selector: 'app-volunteering-feed',
  templateUrl: './volunteering-feed.component.html',
  styleUrls: ['./volunteering-feed.component.css']
})
export class VolunteeringFeedComponent implements OnInit {
  volunteerDto : any;
  interventionAreasTitles : Array<string> = new Array<string>();
  interventionAreas : Array<InterventionArea> = new Array<InterventionArea>();
  currentOffers: Array<Offer> = new Array<Offer>();
  notifications : Array<Notification> =  new Array<Notification>();;
  notificationSentences : Array<string> = new Array<string>();

  constructor(private router : Router, 
    private interventionAreasService: InterventionAreasService, 
    private offerService : OfferService,
    private logoutService : LoginService,
    private volunteerService : VolunteerService,
    private notificationService : NotificationService) { 

    }

  ngOnInit(): void {
    this.getVolunteerData();
    this.getOffers();
  }

  getVolunteerData() {
    this.volunteerService.getVolunteerProfile().subscribe(
      (res:any) => {
        this.volunteerDto = res;

        this.notificationService.getNotificationsByVolunteerId(this.volunteerDto.id).subscribe(
          (res:any) => {
            this.notifications = res;
            var nNotifications = this.notifications.length;
            for (var i = 0; i < nNotifications; i++) {
              var not = this.notifications[i];
              var idOffer = not.idOffer;
              var isNewOffer = not.isNewOffer;

              if (isNewOffer) { // nova proposta
                this.offerService.getById(idOffer).subscribe(
                  (res:any) => { this.notificationSentences.push(res.title) },
                  err => { console.log(err) }
                );
              } else { // questionario
                this.offerService.getById(idOffer).subscribe(
                  (res:any) => { this.notificationSentences.push(res.organizationName) },
                  err => { console.log(err) }
                );
              }


            }
          },
          err => {console.log(err)}
        );

        var interventionAreasIds : number[] = this.volunteerDto.interventionAreasIds;
        var nAreas = interventionAreasIds.length;
  
        for (var i = 0; i < nAreas; i++) {
          var areaId = interventionAreasIds[i];
          this.interventionAreasService.getInterventionAreaById(areaId).subscribe(
            (res:any) => { 
              this.interventionAreas.push(res);
            },
            err => {
              console.log(err)
            }
          );
        };

      },
      err => {

      }
    );
  }

  onInterventionAreaSelection(event:any) {
    var areaTitle = (document.getElementById("filtro_area") as HTMLInputElement).value;
    if (areaTitle == "Todos") {
      this.offerService.getAll().subscribe(
        (res:any) => {
          this.currentOffers = res
        }, 
        err => {
          console.log(err)
        }
      );
    } else {
      this.offerService.getOffersV(areaTitle).subscribe(
        (res:any) => {
          this.currentOffers = res;
        },
        err => {
          console.log(err)
        }
      );
    }
  }

  getOffers() {
    this.offerService.getAllValidatedOffers().subscribe(
      (res:any) => {
        this.currentOffers = res;
      }
    );
  }

  offerDetails(offerId : number) {
    this.router.navigate(['/vOfferDetails',offerId]);
  }

  redirectToApplicationList() {
    this.router.navigate(['/vApplications',this.volunteerDto.id]);
  }

  onLogout() {
    this.logoutService.logout();
  }

  redirectToProfile() {
    this.router.navigate(['/vProfile',this.volunteerDto.id]);
  }

  hideWelcomeModal() {
    if (localStorage.getItem('token') != null) 
      return true;
    else
      return false;
  }

  readNewOfferNotification(notification : any) {
    this.offerDetails(notification.idOffer);
    this.notificationService.read(notification.id).subscribe(
      res => {console.log("Notificacao lida!")},
      err => {console.log(err)}
    );
  }
}
