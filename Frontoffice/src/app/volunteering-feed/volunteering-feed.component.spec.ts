import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VolunteeringFeedComponent } from './volunteering-feed.component';

describe('VolunteeringFeedComponent', () => {
  let component: VolunteeringFeedComponent;
  let fixture: ComponentFixture<VolunteeringFeedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VolunteeringFeedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VolunteeringFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
