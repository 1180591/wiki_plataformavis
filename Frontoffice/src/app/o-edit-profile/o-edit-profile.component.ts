import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../services/login/login.service';
import { OrganizationService } from '../services/organization/organization.service';

@Component({
  selector: 'app-o-edit-profile',
  templateUrl: './o-edit-profile.component.html',
  styleUrls: ['./o-edit-profile.component.css']
})
export class OEditProfileComponent implements OnInit {

  currentOrganization : any;
  
  currentName : any;
  currentLocation : any;
  currentOperatingCountries : any;
  currentWebsiteUrl : any;
  currentAdditionalInfo : any;
  currentRepresentativeName : any;
  currentPhoneNumber : any;
  currentEmailAddress : any;
  currentAddress : any;
  currentZipCode : any;

  constructor(
    private router : Router,
    private organizationService : OrganizationService,
    private logoutService : LoginService
  ) { }

  ngOnInit(): void {
    this.getOrganizationData();
  }

  getOrganizationData() {
    this.organizationService.getOrganizationProfile().subscribe(
      res => {
        this.currentOrganization = res;
        this.currentName = this.currentOrganization.name;
        this.currentLocation = this.currentOrganization.location;
        this.currentOperatingCountries = this.currentOrganization.operatingCountries;
        this.currentWebsiteUrl = this.currentOrganization.websiteUrl;
        this.currentAdditionalInfo = this.currentOrganization.additionalInfo;
        this.currentRepresentativeName = this.currentOrganization.representativeName;
        this.currentPhoneNumber = this.currentOrganization.phoneNumber;
        this.currentEmailAddress = this.currentOrganization.emailAddress;
        this.currentAddress = this.currentOrganization.address;
        this.currentZipCode = this.currentOrganization.postalCode;
      },
      err => { console.log(err) }
    );
  }

  redirectToFeed() {
    this.router.navigateByUrl('/organizationFeed');
  }

  redirectToProfile() {
    this.router.navigate(['/oProfile',this.currentOrganization.id]);
  }

  logout() {
    this.logoutService.logout();
  }

  editProfile() {
    var dto = {
      Id : this.currentOrganization.id,
      EmailAddress : this.currentEmailAddress,
      Name : this.currentName,
      OperatingCountries : this.currentOperatingCountries,
      WebsiteUrl : this.currentWebsiteUrl,
      AdditionalInfo : this.currentAdditionalInfo,
      RepresentativeName : this.currentRepresentativeName,
      PhoneNumber : this.currentPhoneNumber,
      Address : this.currentAddress,
      PostalCode : this.currentZipCode,
      Location : this.currentLocation
    }

    console.log(dto);

    this.organizationService.editOrganizationProfile(dto).subscribe(
      res => {
        this.router.navigate(['/oProfile',this.currentOrganization.id]);
      },
      err => { console.log(err) }
    );
  }

  changeName() { this.currentName = (document.getElementById("nome") as HTMLInputElement).value; }
  changeLocation() { this.currentLocation = (document.getElementById("localidade") as HTMLInputElement).value; }
  changeOperatingCountries() { this.currentOperatingCountries = (document.getElementById("pais_atua") as HTMLInputElement).value; }
  changeWebsiteUrl() { this.currentWebsiteUrl = (document.getElementById("website") as HTMLInputElement).value; }
  changeAdditionalInfo() { this.currentAdditionalInfo = (document.getElementById("info_adicionais") as HTMLInputElement).value; }
  changeRepresentativeName() { this.currentRepresentativeName = (document.getElementById("representante") as HTMLInputElement).value; }
  changePhoneNumber() { this.currentPhoneNumber = (document.getElementById("telemovel_entidade") as HTMLInputElement).value; }
  changeEmailAddress() { this.currentEmailAddress = (document.getElementById("email_entidade") as HTMLInputElement).value; }
  changeAddress() { this.currentAddress = (document.getElementById("morada") as HTMLInputElement).value; }
  changeZipCode() { this.currentAddress = (document.getElementById("codigo_postal") as HTMLInputElement).value; }


}
