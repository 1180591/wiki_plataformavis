import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OEditProfileComponent } from './o-edit-profile.component';

describe('OEditProfileComponent', () => {
  let component: OEditProfileComponent;
  let fixture: ComponentFixture<OEditProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OEditProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OEditProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
