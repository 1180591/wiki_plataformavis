import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OOfferDetailsComponent } from './o-offer-details.component';

describe('OOfferDetailsComponent', () => {
  let component: OOfferDetailsComponent;
  let fixture: ComponentFixture<OOfferDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OOfferDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OOfferDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
