import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Application from '../model/Application';
import InterventionArea from '../model/InterventionArea';
import Offer from '../model/Offer';
import Volunteer from '../model/Volunteer';
import { ApplicationService } from '../services/application/application.service';
import { InterventionAreasService } from '../services/interventionAreas/intervention-areas.service';
import { OfferService } from '../services/offer/offer.service';
import { SchoolService } from '../services/school/school.service';
import { VolunteerService } from '../services/volunteer/volunteer.service';

@Component({
  selector: 'app-o-offer-details',
  templateUrl: './o-offer-details.component.html',
  styleUrls: ['./o-offer-details.component.css']
})
export class OOfferDetailsComponent implements OnInit {
  offer : any;
  currentCandidates: Array<Volunteer> = new Array<Volunteer>();
  currentCandidatesSchools : Array<string> = new Array<string>();
  interventionAreaTitle : any;

  constructor(private route: ActivatedRoute, 
    private offerService: OfferService,
    private applicationService: ApplicationService,
    private volunteerService: VolunteerService,
    private interventionAreasService: InterventionAreasService,
    private schoolService: SchoolService,
    private router: Router) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const offerIdFromRoute = Number(routeParams.get('offerId'));
    console.log(offerIdFromRoute);

    // this.offerService.checkStatus(offerIdFromRoute).subscribe(
    //   status => { this.offerStatus = status; console.log("Offer status: "+status); },
    //   err => { console.log(err) }
    // );
    
    this.offerService.getById(offerIdFromRoute).subscribe(
      (offer:any) => {
        this.offer = offer;

        this.interventionAreasService.getInterventionAreaById(this.offer.interventionAreaId).subscribe(
          (res:any) => { this.interventionAreaTitle = res.tituloOrganizacao; }
        );

        this.applicationService.getByOffer(offerIdFromRoute).subscribe(
          (applicationsList:any) => {
            var nOffers = applicationsList.length;
            for (var i = 0; i < nOffers; i++) {
              var volunteerId = applicationsList[i].idVolunteer;
              this.volunteerService.getVolunteerById(volunteerId).subscribe(
                (volunteerDto:any) => {
                  this.currentCandidates.push(volunteerDto);
                  this.schoolService.getDegreeById(volunteerDto.degreeId).subscribe(
                    (res:any) => { this.currentCandidatesSchools.push(res.schoolName) },
                    err => { console.log(err); }
                  );
                },
                (err:any) => { console.log(err) }
              );
            }


          },
          err => { console.log(err) }
        );
      },
      err => { console.log(err)}
    );
  }

  redirectToFeed() {
    this.router.navigateByUrl("/organizationFeed");
  }

  redirectToProfile() {
    this.router.navigate(["/oProfile",this.offer.idOrganization]);
  }

  onLogout() {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }

  editOffer() {
    this.router.navigate(['editOffer',this.offer.id]);
  }

}
