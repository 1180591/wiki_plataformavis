import { Component, OnInit, SystemJsNgModuleLoader } from '@angular/core';
import { discardPeriodicTasks } from '@angular/core/testing';
import { Router } from '@angular/router';
import InterventionArea from '../model/InterventionArea';
import Offer from '../model/Offer';
import { InterventionAreasService } from '../services/interventionAreas/intervention-areas.service';
import { LoginService } from '../services/login/login.service';
import { OfferService } from '../services/offer/offer.service';
import { OrganizationService } from '../services/organization/organization.service';

@Component({
  selector: 'app-organization-feed',
  templateUrl: './organization-feed.component.html',
  styleUrls: ['./organization-feed.component.css']
})
export class OrganizationFeedComponent implements OnInit {
  organizationDto : any;
  interventionAreas : Array<InterventionArea> = new Array<InterventionArea>();
  currentOffers: Array<Offer> = new Array<Offer>();

  constructor(
    private router: Router, 
    private interventionAreasService: InterventionAreasService, 
    private organizationService : OrganizationService,
    private offerService : OfferService,
    private logoutService: LoginService) { }

    ngOnInit(): void {
      this.getOrganizationData();
      this.getOffers();
    }

    getOffers() {
      this.offerService.getAll().subscribe(
        (res:any) => {
          this.currentOffers = res;
        }
      );
    }

    getOrganizationData() {
      // Atualiza as variaveis organizationDto e areasIntervencao
          // organizationDto e utilizado ao submeter a oferta, por exemplo, sendo necessario o Id da organizacao
          // areasIntervencao e uma lista utilizada no menu drop-down de areas de intervencao
      this.organizationService.getOrganizationProfile().subscribe(
        (res:any) => { 
          this.organizationDto = res;
          var interventionAreasIds : number[] = this.organizationDto.interventionAreasIds;
          var nAreas = interventionAreasIds.length;
  
          for (var i = 0; i < nAreas; i++) {
            var areaId = interventionAreasIds[i];
            console.log("Area com ID "+areaId+"...");
            this.interventionAreasService.getInterventionAreaById(areaId).subscribe(
              (res:any) => { 
                console.log("Titulo da area: "+res.tituloVoluntario);
                this.interventionAreas.push(res);
              },
              err => {
                console.log(err)
              }
            );
          };
        },
        err => { console.log(err) }
      );
    }

    onInterventionAreaSelection(event:any) {
      var areaTitle = (document.getElementById("filtro_area") as HTMLInputElement).value;
      if (areaTitle == "Todos") {
        this.offerService.getAll().subscribe(
          (res:any) => {
            this.currentOffers = res
          }, 
          err => {
            console.log(err)
          }
        );
      } else {
        this.offerService.getOffersO(this.organizationDto.id, areaTitle).subscribe(
          (res:any) => {
            this.currentOffers = res
          },
          err => {
            console.log(err)
          }
        );
      }
    }

    onLogout() {
      this.logoutService.logout();
    }

    redirectToFeed() {
      this.router.navigateByUrl("/organizationFeed");
    }

    redirectToNewOffer() {
      this.router.navigateByUrl("/newOffer");
    }
}
