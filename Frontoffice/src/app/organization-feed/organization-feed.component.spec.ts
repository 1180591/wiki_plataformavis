import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationFeedComponent } from './organization-feed.component';

describe('OrganizationFeedComponent', () => {
  let component: OrganizationFeedComponent;
  let fixture: ComponentFixture<OrganizationFeedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrganizationFeedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
