import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VProfileOrganizationComponent } from './vprofile-organization.component';

describe('VProfileOrganizationComponent', () => {
  let component: VProfileOrganizationComponent;
  let fixture: ComponentFixture<VProfileOrganizationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VProfileOrganizationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VProfileOrganizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
