import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import InterventionArea from '../model/InterventionArea';
import Notification from '../model/Notification';
import { InterventionAreasService } from '../services/interventionAreas/intervention-areas.service';
import { OfferService } from '../services/offer/offer.service';
import { OrganizationService } from '../services/organization/organization.service';

@Component({
  selector: 'app-new-offer',
  templateUrl: './new-offer.component.html',
  styleUrls: ['./new-offer.component.css']
})
export class NewOfferComponent implements OnInit {
  organizationDto : any;
  selectedInterventionArea : number = 0;
  interventionAreas : Array<InterventionArea> = new Array<InterventionArea>();

  formModel = {
    Title : '',
    Description : '',
    StartDate : '',
    EndDate: '',
    ExpectedVolunteeringHours: 0,
    Location: '',
    RolesToPlay: '',
    AdditionalInfo: '',
    InterventionAreaId: 0
  };

  constructor(
    private organizationService: OrganizationService, 
    private interventionAreasService: InterventionAreasService, 
    private offerService: OfferService,
    private router: Router) { }

  ngOnInit(): void {
    // Atualiza as variaveis organizationDto e areasIntervencao
        // organizationDto e utilizado ao submeter a oferta, por exemplo, sendo necessario o Id da organizacao
        // areasIntervencao e uma lista utilizada no menu drop-down de areas de intervencao
    this.organizationService.getOrganizationProfile().subscribe(
      (res:any) => { 
        this.organizationDto = res;
        var interventionAreasIds : number[] = this.organizationDto.interventionAreasIds;
        var nAreas = interventionAreasIds.length;

        for (var i = 0; i < nAreas; i++) {
          var areaId = interventionAreasIds[i];          
          this.interventionAreasService.getInterventionAreaById(areaId).subscribe(
            (res:any) => { 
              this.interventionAreas.push(res);
            },
            err => {
              console.log(err)
            }
          );
        }
      },
      err => { console.log(err) }
    );
  }

  onLogout() {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }

  redirectToFeed() {
    this.router.navigate(['/organizationFeed']);
  }

  onSubmit(form:NgForm) {
    var formValue = form.value;
    var dto = {
      IdOrganization : this.organizationDto.id,
      Title : formValue.Title,
      Description : formValue.Description,
      StartDate : formValue.StartDate,
      EndDate : formValue.EndDate,
      ExpectedVolunteeringHours : formValue.ExpectedVolunteeringHours,
      Location : formValue.Location,
      RolesToPlay : formValue.RolesToPlay,
      AdditionalInfo : formValue.AdditionalInfo,
      InterventionAreaId : this.selectedInterventionArea
    }
    
    this.offerService.addOffer(dto).subscribe(
      res => { 
        this.router.navigateByUrl('/organizationFeed');
      },
      err => { console.log(err) }
    );
  }

  onInterventionAreaSelection(event:any) {
    this.selectedInterventionArea = parseInt((document.getElementById("interventionArea") as HTMLInputElement).value);
  }

}
