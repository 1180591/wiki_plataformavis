import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Application from '../model/Application';
import Volunteer from '../model/Volunteer';
import { ApplicationService } from '../services/application/application.service';
import { InterventionAreasService } from '../services/interventionAreas/intervention-areas.service';
import { OfferService } from '../services/offer/offer.service';
import { SchoolService } from '../services/school/school.service';
import { VolunteerService } from '../services/volunteer/volunteer.service';

@Component({
  selector: 'app-a-offer-details',
  templateUrl: './a-offer-details.component.html',
  styleUrls: ['./a-offer-details.component.css']
})
export class AOfferDetailsComponent implements OnInit {

  offer : any;
  interventionAreaTitle : any;
  currentCandidates: Array<Volunteer> = new Array<Volunteer>();
  currentCandidatesSchools : Array<string> = new Array<string>();
  applicationsList : Array<Application> = new Array<Application>();

  constructor(private route: ActivatedRoute, 
    private offerService: OfferService,
    private interventionAreasService: InterventionAreasService,
    private applicationService: ApplicationService,
    private volunteerService: VolunteerService,
    private schoolService: SchoolService,
    private router: Router) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const offerIdFromRoute = Number(routeParams.get('offerId'));
    console.log(offerIdFromRoute);

    // this.offerService.checkStatus(offerIdFromRoute).subscribe(
    //   status => { this.offerStatus = status; console.log("Offer status: "+status); },
    //   err => { console.log(err) }
    // );
    
    this.offerService.getById(offerIdFromRoute).subscribe(
      (offer:any) => {
        this.offer = offer;

        this.interventionAreasService.getInterventionAreaById(this.offer.interventionAreaId).subscribe(
          (res:any) => { this.interventionAreaTitle = res.tituloOrganizacao; }
        );

        this.applicationService.getByOffer(offerIdFromRoute).subscribe(
          (applicationsList:any) => {
            this.applicationsList = applicationsList;
            var nOffers = this.applicationsList.length;
            for (var i = 0; i < nOffers; i++) {
              var volunteerId = applicationsList[i].idVolunteer;
              this.volunteerService.getVolunteerById(volunteerId).subscribe(
                (volunteerDto:any) => {
                  this.currentCandidates.push(volunteerDto);
                  this.schoolService.getDegreeById(volunteerDto.degreeId).subscribe(
                    (res:any) => { this.currentCandidatesSchools.push(res.schoolName) },
                    err => { console.log(err); }
                  );
                },
                (err:any) => { console.log(err) }
              );
            }


          },
          err => { console.log(err) }
        );
      },
      err => { console.log(err)}
    );
  }

  redirectToOrganizationProfile() {}
  redirectToVolunteerProfile() {}
  redirectToOffers() {}
  redirectToOrganizations() {}
  redirectToVolunteers() {}
  redirectToMetrics() {}
  onLogout() {}

  validateOffer() {
    this.offerService.validateOffer(this.offer).subscribe(
      res => {
        this.router.navigateByUrl('/aOffers');
      },
      err => { console.log(err); }
    );
  }

  rejectOffer() {}
}
