import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AOfferDetailsComponent } from './a-offer-details.component';

describe('AOfferDetailsComponent', () => {
  let component: AOfferDetailsComponent;
  let fixture: ComponentFixture<AOfferDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AOfferDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AOfferDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
