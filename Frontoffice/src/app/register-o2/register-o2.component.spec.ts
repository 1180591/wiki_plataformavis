import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterO2Component } from './register-o2.component';

describe('RegisterO2Component', () => {
  let component: RegisterO2Component;
  let fixture: ComponentFixture<RegisterO2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterO2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterO2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
