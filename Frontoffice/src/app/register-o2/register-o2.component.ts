import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-o2',
  templateUrl: './register-o2.component.html',
  styleUrls: ['./register-o2.component.css']
})
export class RegisterO2Component implements OnInit {

  constructor(private router : Router) { }

  ngOnInit(): void {
  }

  registrationFinished() {
    this.router.navigateByUrl("/organizationFeed");
  }

}
