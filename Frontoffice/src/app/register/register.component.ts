import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  redirectToVolunteerRegister() {
    this.router.navigateByUrl("/registerVolunteer1");
  }

  redirectToOrganizationRegister() {
    this.router.navigateByUrl("/registerOrganization1");
  }

  redirectToLogin() {
    this.router.navigateByUrl("/login");
  }
}
