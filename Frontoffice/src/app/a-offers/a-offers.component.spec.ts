import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AOffersComponent } from './a-offers.component';

describe('AOffersComponent', () => {
  let component: AOffersComponent;
  let fixture: ComponentFixture<AOffersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AOffersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AOffersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
