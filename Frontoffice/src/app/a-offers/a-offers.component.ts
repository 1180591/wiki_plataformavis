import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Offer from '../model/Offer';
import { LoginService } from '../services/login/login.service';
import { OfferService } from '../services/offer/offer.service';

@Component({
  selector: 'app-a-offers',
  templateUrl: './a-offers.component.html',
  styleUrls: ['./a-offers.component.css']
})
export class AOffersComponent implements OnInit {

  offersList : Array<Offer> = new Array<Offer>();

  constructor(
    private router : Router,
    private logoutService : LoginService,
    private offerService : OfferService
  ) { }

  ngOnInit(): void {
    this.offerService.getAll().subscribe(
      (res:Offer[]) => { this.offersList = res; console.log(res) },
      err => { console.log(err); } 
    );
  }

  redirectToOrganizationsList() {
    this.router.navigateByUrl('/aOrganizations');
  }

  redirectToVolunteersList() {
    this.router.navigateByUrl('/aVolunteers');
  }

  redirectToMetrics() {
    this.router.navigateByUrl('/aMetrics');
  }

  logout() {
    this.logoutService.logout();
  }

  refresh() {
    this.router.navigateByUrl('/aOffers');
  }

  offerDetails(id : number) {
    this.offerService.getById(id).subscribe(
      res => { this.router.navigate(['/aOfferDetails',id]) },
      err => {}
    );
  }
}
