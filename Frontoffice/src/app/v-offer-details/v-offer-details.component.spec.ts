import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VOfferDetailsComponent } from './v-offer-details.component';

describe('VOfferDetailsComponent', () => {
  let component: VOfferDetailsComponent;
  let fixture: ComponentFixture<VOfferDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VOfferDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VOfferDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
