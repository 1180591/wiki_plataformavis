import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Notification from '../model/Notification';
import Offer from '../model/Offer';
import { ApplicationService } from '../services/application/application.service';
import { NotificationService } from '../services/notification/notification.service';
import { OfferService } from '../services/offer/offer.service';
import { OrganizationService } from '../services/organization/organization.service';
import { VolunteerService } from '../services/volunteer/volunteer.service';

@Component({
  selector: 'app-v-offer-details',
  templateUrl: './v-offer-details.component.html',
  styleUrls: ['./v-offer-details.component.css']
})
export class VOfferDetailsComponent implements OnInit {
  offer : any;
  offerIdFromRoute:any;
  volunteerDto : any;
  applicationNotSent : boolean = true;
  notifications : Array<Notification> =  new Array<Notification>();
  notificationSentences : Array<string> = new Array<string>();

  constructor(private route: ActivatedRoute, 
    private offerService: OfferService,
    private router: Router,
    private applicationService: ApplicationService,
    private volunteerService : VolunteerService,
    private notificationService : NotificationService) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    this.offerIdFromRoute = Number(routeParams.get('offerId'));

    this.offerService.getById(this.offerIdFromRoute).subscribe(
      (res:any) => {
        this.offer = res;
        this.volunteerService.getVolunteerProfile().subscribe(
          (res:any) => {
            this.volunteerDto = res;

            this.notificationService.getNotificationsByVolunteerId(this.volunteerDto.id).subscribe(
              (res:any) => {
                this.notifications = res;
                var nNotifications = this.notifications.length;
                for (var i = 0; i < nNotifications; i++) {
                  var not = this.notifications[i];
                  var idOffer = not.idOffer;
                  var isNewOffer = not.isNewOffer;
    
                  if (isNewOffer) { // nova proposta
                    this.offerService.getById(idOffer).subscribe(
                      (res:any) => { this.notificationSentences.push(res.title) },
                      err => { console.log(err) }
                    );
                  } else { // questionario
                    this.offerService.getById(idOffer).subscribe(
                      (res:any) => { this.notificationSentences.push(res.organizationName) },
                      err => { console.log(err) }
                    );
                  }
    
    
                }
              },
              err => {console.log(err)}
            );

            this.applicationService.getByOfferAndVolunteer(this.offer.id, this.volunteerDto.id).subscribe(
              (res:any) => {
                console.log(res);
                if (res != null) { this.applicationNotSent = false; }
                console.log(this.applicationNotSent);
              },
              (err:any) => { console.log(err) }
            );
          },
          err => {
            console.log(err)
          }
        );
      },
      err => {
        console.log(err)
      }
    );
  }

  getOfferData() {
    this.offerService.getById(this.offerIdFromRoute).subscribe(
      (res:any) => {
        this.offer = res;
      },
      err => {
        console.log(err)
      }
    );
  }

  getVolunteerData() {
    this.volunteerService.getVolunteerProfile().subscribe(
      (res:any) => {
        this.volunteerDto = res;
      },
      err => {
        console.log(err)
      }
    );
  }

  onApplicationSubmit() {
    console.log("Click enviar candidatura. ("+this.offerIdFromRoute+")");

    var dto =  {
      idOffer : this.offerIdFromRoute
    };

    this.applicationService.submit(dto).subscribe(
      res => {
        this.router.navigateByUrl("/volunteeringFeed");
      },
      err => {
        console.log(err)
      }
    );
  }

  openModal() {
    (<HTMLInputElement>document.getElementById("modal-voluntario-candidatura")).style.display = "flex";
  }

  closeModal() {
    (<HTMLInputElement>document.getElementById("modal-voluntario-candidatura")).style.display = "none";
  }

  redirectToFeed() {
    this.router.navigateByUrl("/volunteeringFeed");
  }

  onLogout() {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }

  redirectToApplicationList() {
    console.log("!!!");
    this.router.navigate(['/vApplications',this.volunteerDto.id]);
  }

  redirectToProfile() {
    this.router.navigate(['/vProfile',this.volunteerDto.id]);
  }

  offerDetails(offerId : number) {
    this.router.navigate(['/vOfferDetails',offerId]);
  }

  readNewOfferNotification(notification : any) {
    this.offerDetails(notification.idOffer);
    this.notificationService.read(notification.id).subscribe(
      res => {console.log("Notificacao lida!")},
      err => {console.log(err)}
    );
  }
}
