import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-v3',
  templateUrl: './register-v3.component.html',
  styleUrls: ['./register-v3.component.css']
})
export class RegisterV3Component implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  redirectToNextPage() {
    this.router.navigateByUrl("/registerVolunteer4");
  }

}
