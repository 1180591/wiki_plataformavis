import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AMetricsComponent } from './a-metrics.component';

describe('AMetricsComponent', () => {
  let component: AMetricsComponent;
  let fixture: ComponentFixture<AMetricsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AMetricsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AMetricsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
