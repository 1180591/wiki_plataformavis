import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-a-metrics',
  templateUrl: './a-metrics.component.html',
  styleUrls: ['./a-metrics.component.css']
})
export class AMetricsComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  redirectToOffersList() {
    this.router.navigateByUrl('/aOffers');
  }
}
