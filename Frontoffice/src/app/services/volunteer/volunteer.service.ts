import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Volunteer from 'src/app/model/Volunteer';

@Injectable({
  providedIn: 'root'
})
export class VolunteerService {
  private backendUrl = 'https://localhost:30606/volunteer';

  constructor(private http: HttpClient) { }

  getVolunteerProfile() {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get(this.backendUrl, {headers: tokenHeader});
  }

  getVolunteerId() {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get(this.backendUrl+'/id', {headers: tokenHeader});
  }

  getVolunteerById(volunteerId: number) {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get(this.backendUrl+'/'+volunteerId, {headers: tokenHeader});
  }

  editVolunteerProfile(v : any) {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.post(this.backendUrl+'/edit',v,{headers: tokenHeader});
  }

  getAll() {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get(this.backendUrl+'/all', {headers: tokenHeader});
  }
}
