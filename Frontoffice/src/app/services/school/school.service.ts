import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SchoolService {

  constructor(private http : HttpClient, private router: Router) { }
  private backendUrl = 'https://localhost:30606';

  getDegreeById(id : number) {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get(this.backendUrl+'/school/degree?id='+id,{ headers: tokenHeader});
  }

  getAllSchools() {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get(this.backendUrl+'/school/schools',{ headers: tokenHeader});
  }

  getAllDegrees() {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get(this.backendUrl+'/school/degrees',{ headers: tokenHeader});
  }
}
