import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import Offer from 'src/app/model/Offer';

@Injectable({
  providedIn: 'root'
})
export class OfferService {

  constructor(private http : HttpClient, private formBuilder: FormBuilder) { }
  private backendUrl = 'https://localhost:30606/offer';

  addOffer(dto: any) {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.post(this.backendUrl, dto, {headers: tokenHeader});
  }

  validateOffer(dto: any) {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.post(this.backendUrl+'/validate', dto, {headers: tokenHeader});
  }

  getAllValidatedOffers() {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get(this.backendUrl+"/vOffers",{ headers: tokenHeader});
  }

  getOffersO(organizationId:number, interventionAreaTitle:string) {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get(this.backendUrl+"/offersO?idOrganization="+organizationId+"&areaTitle="+interventionAreaTitle,{ headers: tokenHeader});
  }

  getOffersByOrganizationId(organizationId:number) {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get(this.backendUrl+"/orgOffers?idOrganization="+organizationId,{ headers: tokenHeader});
  }

  getOffersV(interventionAreaTitle:string) {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get(this.backendUrl+"/offersV?areaTitle="+interventionAreaTitle,{ headers: tokenHeader});
  }

  getAll() : Observable<Offer[]> {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get<Offer[]>(this.backendUrl,{ headers: tokenHeader});
  }

  getById(id : number) {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get(this.backendUrl+'/'+id,{ headers: tokenHeader});
  }

  editOffer(dto: any) {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.post(this.backendUrl+'/edit',dto,{ headers: tokenHeader});
  }
  
  deleteOffer(id : number) { 
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.post(this.backendUrl+'/delete',id,{ headers: tokenHeader});
  }

  checkStatus(offerId : number) {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get(this.backendUrl+'/status?offerId='+offerId,{ headers: tokenHeader});
  }

  offerAlreadyStarted(currentY:number, currentM:number, currentD:number, offerY:number, offerM:number, offerD:number) {
    if (offerY < currentY) {
      return true;
    } else {
      if (offerM < currentM) {
        return true;
      } else {
        if (offerD < currentD) {
          return true;
        } else {
          return false;
        }
      }
    }
  }

  offerAlreadyEnded(currentY:number, currentM:number, currentD:number, offerY:number, offerM:number, offerD:number) {
    if (offerY > currentY) {
      return false;
    } else {
      if (offerM > currentM) {
        return false;
      } else {
        if (offerD > currentD) {
          return false;
        } else {
          return true;
        }
      }
    }
  }
}
