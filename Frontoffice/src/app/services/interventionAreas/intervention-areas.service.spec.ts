import { TestBed } from '@angular/core/testing';

import { InterventionAreasService } from './intervention-areas.service';

describe('InterventionAreasService', () => {
  let service: InterventionAreasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InterventionAreasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
