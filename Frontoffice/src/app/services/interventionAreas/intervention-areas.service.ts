import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class InterventionAreasService {

  constructor(private http : HttpClient) { }
  private backendUrl = 'https://localhost:30606';

  getInterventionAreaById(id : number) {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    var url = this.backendUrl+'/interventionArea/'+id;
    return this.http.get(url, {headers : tokenHeader});
  }
  
}
