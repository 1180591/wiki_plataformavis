import { Injectable } from '@angular/core';
import { VolunteerRegistration } from 'src/app/model/VolunteerRegistration';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  private backendUrl = 'http://localhost:5001/register';

  constructor(private http : HttpClient) { }

  addVolunteer(registo : VolunteerRegistration): Observable<VolunteerRegistration> {
    return this.http.post<VolunteerRegistration>(this.backendUrl, registo);
  }
}


