import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class InternationalInterestService {
  private backendUrl = 'https://localhost:30606/internationalInterest';

  constructor(private http: HttpClient) { }

  getAnswers() {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get(this.backendUrl, {headers: tokenHeader});
  }
}
