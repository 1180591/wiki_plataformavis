import { TestBed } from '@angular/core/testing';

import { InternationalInterestService } from './international-interest.service';

describe('InternationalInterestService', () => {
  let service: InternationalInterestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InternationalInterestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
