import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import Application from 'src/app/model/Application';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  constructor(private http : HttpClient) { }
  private backendUrl = 'https://localhost:30606/application';

  submit(dto:any) {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.post(this.backendUrl,dto,{headers:tokenHeader});
  }

  getByOffer(idOffer:number) {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')})
    return this.http.get(this.backendUrl+"/organization?idOffer="+idOffer,{headers:tokenHeader});
  }

  getByVolunteerId(idVolunteer:number) : Observable<Application[]> {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')})
    return this.http.get<Application[]>(this.backendUrl+"/volunteer?idVolunteer="+idVolunteer,{headers:tokenHeader});
  }

  getByOfferAndVolunteer(idOffer:number,idVolunteer:number) {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')})
    return this.http.get(this.backendUrl+"/check?idOffer="+idOffer+"&idVolunteer="+idVolunteer,{headers:tokenHeader});
  }
}
