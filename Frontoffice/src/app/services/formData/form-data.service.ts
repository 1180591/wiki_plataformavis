import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FormDataService {

  // Pagina 1
  name : string;
  gender: string;
  birthday: string;
  nationality: string;
  idDoc: string;
  idNumber: string;
  taxpayerNumber: string;
  email: string;
  phoneNumber: string;
  postalAddress: string;
  zipCode: string;
  locality: string;
  emergencyPhone: string;
  password: string;

  // Pagina 2
  mechanographicNumber : string;
  school : string;
  course : string;
  experience : string;
  reasons_satisfaction : boolean;
  reasons_skills : boolean;
  reasons_professional : boolean;
  reasons_experience : boolean;
  reasons_help : boolean;
  reasons_network : boolean;
  reasons_freetime : boolean;

  constructor() { 
    this.name = '';
    this.gender = '';
    this.birthday = '';
    this.nationality = '';
    this.idDoc = '';
    this.idNumber = '';
    this.taxpayerNumber = '';
    this.email = '';
    this.phoneNumber = '';
    this.postalAddress = '';
    this.zipCode = '';
    this.locality = '';
    this.emergencyPhone = '';
    this.password = '';
    this.mechanographicNumber = '';
    this.school = '';
    this.course = '';
    this.experience = '';
    this.reasons_satisfaction = true;
    this.reasons_skills = true;
    this.reasons_professional = true;
    this.reasons_experience = true;
    this.reasons_help = true;
    this.reasons_network = true;
    this.reasons_freetime = true;
  }
}
