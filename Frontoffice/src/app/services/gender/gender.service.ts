import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GenderService {
  private backendUrl = 'https://localhost:30606/gender';

  constructor(private http: HttpClient) { }

  getGenders() {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get(this.backendUrl, {headers: tokenHeader});
  }
}
