import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  
  constructor(private http : HttpClient, private router: Router, private formBuilder : FormBuilder) { }
  private backendUrl = 'https://localhost:30606';
  
  formModel = this.formBuilder.group({
    Email : ['', Validators.required],
    Password : ['', Validators.required]
  });
  
  login(formData: any) {
    return this.http.post(this.backendUrl + '/login', formData);
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['/home']);
  }
  
}
