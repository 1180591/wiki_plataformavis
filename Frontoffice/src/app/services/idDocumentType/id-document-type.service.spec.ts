import { TestBed } from '@angular/core/testing';

import { IdDocumentTypeService } from './id-document-type.service';

describe('IdDocumentTypeService', () => {
  let service: IdDocumentTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IdDocumentTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
