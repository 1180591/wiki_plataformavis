import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class IdDocumentTypeService {
  private backendUrl = 'https://localhost:30606/idDocument';

  constructor(private http: HttpClient) { }

  getDocumentTypes() {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get(this.backendUrl, {headers: tokenHeader});
  }
}
