import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {
  private backendUrl = 'https://localhost:30606/organization';

  constructor(private http: HttpClient) { }

  getOrganizationProfile() {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get(this.backendUrl, {headers : tokenHeader});
  }

  getOrganizationById(organizationId : number) {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get(this.backendUrl+'/'+organizationId, {headers : tokenHeader});
  }

  editOrganizationProfile(o : any) {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.post(this.backendUrl+'/edit',o,{headers: tokenHeader});
  }

  getAll() {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get(this.backendUrl+'/all', {headers : tokenHeader});
  }
}
