import { HttpClient, HttpHeaders } from '@angular/common/http';
import { not } from '@angular/compiler/src/output/output_ast';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private backendUrl = 'https://localhost:30606/notification';

  constructor(private http: HttpClient) { }

  getNotificationsByVolunteerId(volunteerId : number) {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    return this.http.get(this.backendUrl+'/'+volunteerId, {headers: tokenHeader});
  }

  read(notificationId : number) {
    var tokenHeader = new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('token')});
    console.log(this.backendUrl+'/read/'+notificationId);
    return this.http.post(this.backendUrl+'/read/'+notificationId,{headers: tokenHeader});
  }
}
