import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import InterventionArea from '../model/InterventionArea';
import { InterventionAreasService } from '../services/interventionAreas/intervention-areas.service';
import { LoginService } from '../services/login/login.service';
import { OfferService } from '../services/offer/offer.service';
import { OrganizationService } from '../services/organization/organization.service';
import { NgForm } from '@angular/forms';
import { AnonymousSubject } from 'rxjs/internal/Subject';

@Component({
  selector: 'app-o-edit-offer',
  templateUrl: './o-edit-offer.component.html',
  styleUrls: ['./o-edit-offer.component.css']
})
export class OEditOfferComponent implements OnInit {

  offer : any;
  organization : any;
  interventionAreas : Array<InterventionArea> = new Array<InterventionArea>();

  currentTitle : any;
  currentDescription : any;
  currentStartDate : any;
  currentEndDate : any;
  currentExpectedVolunteeringHours : any;
  currentLocation : any;
  currentRolesToPlay : any;
  currentAdditionalInfo : any;
  currentInterventionAreaId : any;

  constructor( 
    private route : ActivatedRoute,
    private router : Router,
    private logoutService : LoginService,
    private organizationService : OrganizationService,
    private interventionAreasService : InterventionAreasService,
    private offerService : OfferService
    ) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const offerIdFromRoute = Number(routeParams.get('offerId'));
    this.offerService.getById(offerIdFromRoute).subscribe(
      offer => { 
        this.offer = offer;

        this.currentTitle = this.offer.title;
        this.currentDescription = this.offer.description;
        this.currentStartDate = this.offer.startDate;
        this.currentEndDate = this.offer.endDate;
        this.currentExpectedVolunteeringHours = this.offer.expectedVolunteeringHours;
        this.currentLocation = this.offer.location;
        this.currentRolesToPlay = this.offer.rolesToPlay;
        this.currentAdditionalInfo = this.offer.additionalInfo;
        this.currentInterventionAreaId = this.offer.interventionAreaId;

      },
      err => { console.log(err)}
    );

    this.organizationService.getOrganizationProfile().subscribe(
      res => { 
        this.organization = res;
        var interventionAreasIds : number[] = this.organization.interventionAreasIds;
        var nAreas = interventionAreasIds.length;

        for (var i = 0; i < nAreas; i++) {
          var areaId = interventionAreasIds[i];
          this.interventionAreasService.getInterventionAreaById(areaId).subscribe(
            (res:any) => { 
              this.interventionAreas.push(res);
            },
            err => {
              console.log(err)
            }
          );
        };
  
      },
      err => { console.log(err) }
    );

    // console.log("interventionArea.id = "+this.interventionAreas[0].id); console.log(" | offer.id = "+this.offer.id);
  }

  logout() {
    this.logoutService.logout();
  }

  redirectToProfile() {
    this.router.navigate(['/oProfile',this.organization.id]);
  }

  redirectToFeed() {
    this.router.navigateByUrl('/organizationFeed');
  }

  redirectToOfferDetails() {
    this.router.navigate(['/oOfferDetails',this.offer.id]);
  }

  delete() {
    // Por implementar
  }

  openModal() {
    (<HTMLInputElement>document.getElementById("modal-apagar-proposta")).style.display = "flex";
  }

  closeModal() {
    (<HTMLInputElement>document.getElementById("modal-apagar-proposta")).style.display = "none";
  }

  onInterventionAreaSelection() {
    this.currentInterventionAreaId = parseInt((document.getElementById("InterventionAreaId") as HTMLInputElement).value);
  }

  saveChanges() {    
    var dto = {
      IdOrganization : this.organization.id,
      Id : this.offer.id,
      Title : this.currentTitle,
      Description : this.currentDescription,
      StartDate : this.currentStartDate,
      EndDate : this.currentEndDate,
      ExpectedVolunteeringHours : this.currentExpectedVolunteeringHours,
      Location : this.currentLocation,
      RolesToPlay : this.currentRolesToPlay,
      AdditionalInfo : this.currentAdditionalInfo,
      InterventionAreaId : this.currentInterventionAreaId
    }

    console.log(dto);
    
    this.offerService.editOffer(dto).subscribe(
      res => { 
        this.router.navigateByUrl('/organizationFeed');
      },
      err => { console.log(err) }
    );
  }

  titleChange() { this.currentTitle = (document.getElementById("Title") as HTMLInputElement).value; }
  descriptionChange() { this.currentDescription = (document.getElementById("Description") as HTMLInputElement).value; }
  startDateChange() { this.currentStartDate = (document.getElementById("StartDate") as HTMLInputElement).value; }
  endDateChange() { this.currentEndDate = (document.getElementById("EndDate") as HTMLInputElement).value; }
  expectedVolunteeringHoursChange() { this.currentExpectedVolunteeringHours = parseInt((document.getElementById("ExpectedVolunteeringHours") as HTMLInputElement).value); }
  locationChange() { this.currentLocation = (document.getElementById("Location") as HTMLInputElement).value; }
  rolesToPlayChange() { this.currentRolesToPlay = (document.getElementById("RolesToPlay") as HTMLInputElement).value; }
  additionalInfoChange() { this.currentAdditionalInfo = (document.getElementById("AdditionalInfo") as HTMLInputElement).value; }

}
