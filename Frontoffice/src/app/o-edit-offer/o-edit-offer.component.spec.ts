import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OEditOfferComponent } from './o-edit-offer.component';

describe('OEditOfferComponent', () => {
  let component: OEditOfferComponent;
  let fixture: ComponentFixture<OEditOfferComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OEditOfferComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OEditOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
