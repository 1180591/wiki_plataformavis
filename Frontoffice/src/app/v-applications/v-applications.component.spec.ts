import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VApplicationsComponent } from './v-applications.component';

describe('VApplicationsComponent', () => {
  let component: VApplicationsComponent;
  let fixture: ComponentFixture<VApplicationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VApplicationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VApplicationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
