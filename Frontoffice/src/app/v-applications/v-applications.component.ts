import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Application from '../model/Application';
import InterventionArea from '../model/InterventionArea';
import Notification from '../model/Notification';
import { ApplicationService } from '../services/application/application.service';
import { InterventionAreasService } from '../services/interventionAreas/intervention-areas.service';
import { LoginService } from '../services/login/login.service';
import { NotificationService } from '../services/notification/notification.service';
import { OfferService } from '../services/offer/offer.service';
import { VolunteerService } from '../services/volunteer/volunteer.service';

@Component({
  selector: 'app-v-applications',
  templateUrl: './v-applications.component.html',
  styleUrls: ['./v-applications.component.css']
})
export class VApplicationsComponent implements OnInit {
  vId : any;
  interventionAreasTitles : Array<string> = new Array<string>();
  interventionAreas : Array<InterventionArea> = new Array<InterventionArea>();
  currentApplications: Array<Application> = new Array<Application>();
  notifications : Array<Notification> =  new Array<Notification>();
  notificationSentences : Array<string> = new Array<string>();

  constructor(private router: Router,
    private route: ActivatedRoute,
    private applicationsService: ApplicationService,
    private volunteerService: VolunteerService,
    private interventionAreasService: InterventionAreasService,
    private loginService : LoginService,
    private notificationService : NotificationService,
    private offerService: OfferService) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const offerIdFromRoute = Number(routeParams.get('vId'));
    this.vId = offerIdFromRoute;

    this.getVolunteerData();
    this.getApplications();
  }

  getApplications() {
    this.applicationsService.getByVolunteerId(this.vId).subscribe(
      (res:any) => {
        this.currentApplications = res;
      },
      err => { console.log(err) }
    );
  }

  getVolunteerData() {
    this.volunteerService.getVolunteerById(this.vId).subscribe(
      (res:any) => {
        var interventionAreasIds : number[] = res.interventionAreasIds;
        var nAreas = interventionAreasIds.length;
  
        for (var i = 0; i < nAreas; i++) {
          var areaId = interventionAreasIds[i];
          console.log("Area com ID "+areaId+"...");
          this.interventionAreasService.getInterventionAreaById(areaId).subscribe(
            (res:any) => { 
              console.log("Titulo da area: "+res.interventionArea.tituloVoluntario);
              this.interventionAreas.push(res.interventionArea);
              this.interventionAreasTitles.push(res.interventionArea.tituloVoluntario);
            },
            err => {
              console.log(err)
            }
          );
        };

        this.notificationService.getNotificationsByVolunteerId(this.vId).subscribe(
          (res:any) => {
            this.notifications = res;
            var nNotifications = this.notifications.length;
            for (var i = 0; i < nNotifications; i++) {
              var not = this.notifications[i];
              var idOffer = not.idOffer;
              var isNewOffer = not.isNewOffer;

              if (isNewOffer) { // nova proposta
                this.offerService.getById(idOffer).subscribe(
                  (res:any) => { this.notificationSentences.push(res.title) },
                  err => { console.log(err) }
                );
              } else { // questionario
                this.offerService.getById(idOffer).subscribe(
                  (res:any) => { this.notificationSentences.push(res.organizationName) },
                  err => { console.log(err) }
                );
              }


            }
          },
          err => {console.log(err)}
        );

      },
      err => {

      }
    );
  }

  redirectToFeed() {
    this.router.navigateByUrl("volunteeringFeed");
  }

  logout() {
    this.loginService.logout();
  }

  redirectToProfile() {
    this.router.navigate(['/vProfile',this.vId]);
  }

  offerDetails(offerId : number) {
    this.router.navigate(['/vOfferDetails',offerId]);
  }

  readNewOfferNotification(notification : any) {
    this.offerDetails(notification.idOffer);
    this.notificationService.read(notification.id).subscribe(
      res => {console.log("Notificacao lida!")},
      err => {console.log(err)}
    );
  }
}
