# Enunciado

## Descrição Geral

A Engenho&Obra ONGD (E&O) tem vindo a promover, em parceria com o ISEP, a aplicação da Engenharia ao contexto social para dar respostas efetivas a problemas reais, através da iniciativa “Engenharia Solidária: Somos Agentes Transformadores”. Nesta iniciativa é promovida a participação de estudantes do ISEP numa oferta de formação de desenvolvimento técnico e solidário com averbamento no Diploma de Curso e largamente valorizada no mercado de trabalho.

Neste enquadramento, é apresentada uma proposta de integração de aluno(s) em sede de estágio curricular, com o objetivo de desenvolver uma plataforma Web para gestão de voluntariado e de iniciativas de solidariedade. O estagiário deverá implementar o frontend e o backend do sistema.

## Utilizadores da plataforma

Desde já, perspetiva-se que a aplicação será acedida por vários utilizadores com diferentes papéis, tais como:
* **Voluntário**: pessoa (membro do IPP) que se registou na plataforma com o intuito de consultar propostas de voluntariado e efetuar candidatura(s);
* **Entidade**: organização ou associação responsável por submeter propostas de voluntariado e gerir eventuais candidatos;
* **Gestor**: gestor da plataforma (E&O) responsável por gerir propostas de ação de voluntariado

As interações dos utilizadores supramencionados devem ser precedidas de um processo de autenticação. A utilização da aplicação por outras pessoas está restrita a: 

1. Efetuar registo como Voluntário
2. Abrir uma conta / Submeter candidatura


No primeiro caso, o sistema deve solicitar o nome completo do utilizador, o seu ID do politécnico, contacto telefónico, email, sexo, data de nascimento, nacionalidade, endereço postal, documento identificação (CC, BI ou Passaporte), nº contribuinte, contacto de emergência, unidade orgânica do PPorto (escola, curso e semestre), consentimentos (RGPD) e informações adicioniais (experiência em voluntariado, razões para fazer voluntariado, interesse em propostas nacionais ou internacionais).

No segundo caso, o sistema deve solicitar o nome, número de identificação fiscal e página web da entidade promotora juntamente com o nome e endereço de correio eletrónico do representante da mesma. A entidade será posteriormente validada por um gestor da plataforma que torna-la-á ativa e consequentemente visível aos potenciais candidatos.

## Navegabilidade



#### Voluntário

Após a autenticação do voluntariado, este será redirecionado para a página principal, onde poderá executar as funcionalidades disponíveis pela plataforma como:

- Listar as oportunidades/propostas de voluntariado disponíveis e filtrá-las ou ordená-las consoante os critérios definidos pelos clientes.
- Executar funcionalidades sobre o perfil como alterar palavra-passe, fazer logout, consultar os seus dados

![InteracaoVoluntario](./Multimedia/InteracaoVoluntario.png)

Fig. 1 - Processo de candidatura a uma proposta



![InteracaoVoluntario2](./Multimedia/InteracaoVoluntario2.png)

Fig. 2 - Processos de gestão de conta do voluntário (INCOMPLETO)



#### Entidade

Após abertura de conta e autenticação na plataforma, será apresentada uma interface onde poderá executar as seguintes funcionalidades:

- Submissão de nova proposta de voluntariado
- Gestão de candidatos de proposta ativa



#### Gestor

Como serão normalmente geridos os acessos de gestores? Investigar estado da arte.

Única responsabilidade identificada: validação das submissões de propostas de voluntariado



## Questões em Aberto

- Qual será o processo de validação das entidades que tentam registar-se na plataforma de modo a garantir que são legítimas? 
- Foi referido que deve existir um gestor designado para uma proposta. Deve existir então algum utilizador com a responsabilidade de designar um gestor para cada proposta? Ou devem todos os gestores registados terem o mesmo acesso às submissões de propostas por validar? Um gestor poderia também designar uma submissão a ele mesmo, por exemplo.
- A entidade que submeteu a proposta não pode desativá-la? Só o gestor?



# Calendarização

Duração do projeto: 8 Março a 10 Julho

Falou-se inicialmente de uma fase de análise com duração de 3 semanas. No entanto, e por questões políticas, não foi possível decidir as tecnologias a ser usadas até à 5ª semana.

A ideia inicial era durante o intervalo de 5 semanas entre a 3ª semana de Março e o fim de Abril ser desenvolvido o backoffice da plataforma. Caso se mantenha a duração de 5 semanas, isto poderá ser adiado para 5 Abril - 9 Maio.

O intervalo de 4 semanas a seguir seria dedicado ao frontoffice, terminando portanto a 10 Junho e tendo um mês para ajustes específicos e suplementares.

**Nota:** Estes dados não são oficiais e estão sujeitos a alteração. Foram retirados de uma conversa inicial entre o engenheiro orientador deste estágio, professor Paulo Proença, e o estagiário, José Magalhães.

[Atrás](./Home)
