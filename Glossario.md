# Glossário

| **_Termo_**     | **_Descrição_**                                              |
| :-------------- | :----------------------------------------------------------- |
| **Voluntário**  | Membro da comunidade do politécnico interessado em fazer voluntariado. |
| **Organização** | Entidade promotora de ações de voluntariado e iniciativas de solidariedade, que submeterá novas propostas, populando a plataforma. |
|**Administrador**| Utilizador que valida as propostas submetidas pelas organizações. |
| **Notificação** | Aviso dirigido ao voluntário que pode indicar que surgiu uma nova proposta ou que tem um questionário de satisfação por responder.                                                              |
| **Área de Intervenção** | Área onde intervém uma ou mais entidades e onde voluntários demonstram interesse. E.g.: Voluntariado Animal.                                                              |
| **Proposta** | Iniciativa de solidariedade / Proposta de voluntariado emitida por uma organização.                                                              |
| **Escola** | Instituição do politécnico onde estudam, lecionam ou trabalham os voluntários                                                              |
| **Candidatura** | Confirmação de candidatura de um voluntário a uma proposta de voluntariado ativa.                                                              |
| **Proposta pendente** | Proposta de voluntariado ainda não validada por um administrador.                                                             |
| **Proposta ativa** | Proposta de voluntariado validada por um administrador mas ainda não terminada.                                                              |
| **Proposta inativa** | Proposta de voluntariado validada por um administrador e terminada.                                                             |
|                 |                                                              |
|                 |                                                              |
|                 |                                                              |
|                 |                                                              |
|                 |                                                              |
|                 |                                                              |
|                 |                                                              |
|                 |                                                              |
|                 |                                                              |
|                 |                                                              |
|                 |                                                              |
|                 |                                                              |
|                 |                                                              |
|                 |                                                              |
|                 |                                                              |
|                 |                                                              |
|                 |                                                              |
|                 |                                                              |
|                 |                                                              |
|                 |                                                              |