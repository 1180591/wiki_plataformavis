Pontos a abordar na prox reuniao:

- Rever secção de "Contactos" do perfil da entidade
    - Mistura-se um pouco a informação do representante e da entidade em si. Queremos mostrar os dados do representante da entidade aos voluntários?
    - Localidade 2x

    R: Tudo ok. Não foi preciso mexer em nada.


- Uma proposta de voluntariado tem apenas 1 área de atuação

    R: Tudo ok. Não foi preciso mexer em nada.

- Histórico de voluntariado vazio (perfil v, perfil o, offer details)

    R: Aguardo instruções do lado da E&O.

- Gestão de candidatos
    - O que acontece, graficamente, após seleção/não seleção?
    - A participação de um candidato deve poder ser cancelada?

    R: Aguardo instruções do lado da E&O. Contudo, posso ir avançando com a "seleção" de um voluntário para uma determinada proposta e eventual validação de horas de voluntariado. Nota mental: a seleção pode já estar feita, basta alterar o estado da candidatura. Quanto à validação das horas, aí é que pode ser necessário uma nova entidade ou alterações a uma entidade existente como a candidatura (ValidatedVolunteeringHours - default: 0).


- Newsletter
    - Sem conteúdo
    - Sem autorização para ser enviada

    R: Tudo ok. Basta enviar um email personalizado A CADA 2 SEMANAS que redireciona o remetente para a plataforma.

- Métricas para a qual a plataforma não está preparada a emitir:
    - Voluntários
        - Nº total de horas individuais de voluntariado validadas
    - Entidades
        - Nº total de entidades por nacional e internacional
        - Nº total de horas validadas na plataforma
        - Nº total de horas validadas na plataforma por nacional e internacional

    R: Entidade passa a ter 1 único país de atuação e esse país deve ser escolhido de uma lista de países.

- Termos e Condições

    R: Aguardo instruções do lado da E&O.

- Disponibilidade de voluntário não utilizada
- Areas de interesse de voluntário não editáveis

    R: Aguardo instruções do lado da E&O.

- Registo
    - Autorização newsletter
    - Possibilidade de username

    R: Get to work mf. Aguardo instruções do lado da E&O.

- Admin
    - Rejeitar proposta

    R: Só rejeitar mesmo!

Outros tópicos falados:
- Curso de voluntário passa a Área de formação (text box simples)
- Pode ser selecionado mais do que um candidato à mesma proposta


